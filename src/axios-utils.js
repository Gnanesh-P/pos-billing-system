import axios from 'axios'
const BASE_URL = "https://inventory-pos-backend.herokuapp.com"

export const coreHttpGet = ({ url, config = {} }) => new Promise((resolve, reject) =>
    axios.get(BASE_URL + url, config)
    .then(x => resolve(x))
    .catch(x => reject(x))
)
export const coreHttpPut = ({ url, data, config = {} }) => new Promise((resolve, reject) =>
    axios.put(BASE_URL + url, data, config)
    .then(x => resolve(x))
    .catch(x => reject(x))
)
export const coreHttpPost = ({ url, data, config = {} }) => new Promise((resolve, reject) =>
    axios.post(BASE_URL + url, data, config)
    .then(x => resolve(x))
    .catch(x => reject(x))
)
export const coreHttpDelete = ({ url, config = {} }) => new Promise((resolve, reject) =>
    axios.delete(BASE_URL + url, config)
    .then(x => resolve(x))
    .catch(x => reject(x))
)