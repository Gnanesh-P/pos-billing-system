import React from 'react'
import { Redirect } from 'react-router-dom'

import dashboardRoutes from './views/dashboard/DashboardRoutes'
import utilitiesRoutes from './views/utilities/UtilitiesRoutes'

import materialRoutes from './views/material-kit/MaterialRoutes'
import chartsRoute from './views/charts/ChartsRoute'
import dragAndDropRoute from './views/Drag&Drop/DragAndDropRoute'

import formsRoutes from './views/forms/FormsRoutes'
import mapRoutes from './views/map/MapRoutes'
import BillingRoutes from './views/billing/BillingRoutes'
import InvoiceRoutes from './views/invoice/InvoiceRoutes'
import CustomerInvoiceRoutes from './views/pos/customer_invoice/CustomerInvoiceRoute'
import accountRoutings from './views/account/AccountRoutes'
import InventoryRoutes from './views/inventory/InventoryRoutes'
import CustomerRoutes from './views/customer/CustomerRoutes'
import WarehouseRoutes from './views/warehouse/WarehouseRoutes'
import ProductRoutes from './views/product/ProductRoutes'
import CategoryRoutes from './views/category/CategoryRoutes'
import UnitsRoutes from './views/units/UnitsRoutes'
import SupplierRoutes from './views/supplier/SupplierRoutes'
import LocationRoutes from './views/location/LocationRoutes'
import TransactionRoutes from './views/transaction/TransactionRoutes'
import TaxRoutes from './views/tax/TaxRoutes'
import EmployeeRoutes from './views/employee/EmployeeRoutes'



const redirectRoute = [
    {
        path: '/',
        exact: true,
        component: () => <Redirect to="/dashboard" />,
    },
]

const errorRoute = [
    {
        component: () => <Redirect to="/session/404" />,
    },
]

const routes = [
    ...dashboardRoutes,
    ...CustomerRoutes,
    ...ProductRoutes,
    ...WarehouseRoutes,
    ...CategoryRoutes,
    ...InvoiceRoutes,
    ...BillingRoutes,
    ...UnitsRoutes,
    ...TaxRoutes,
    ...EmployeeRoutes,
    ...SupplierRoutes,
    ...LocationRoutes,
    ...TransactionRoutes,
    ...InventoryRoutes,
    ...accountRoutings,
    ...CustomerInvoiceRoutes,
    ...materialRoutes,
    ...utilitiesRoutes,
    ...chartsRoute,
    ...dragAndDropRoute,
    ...formsRoutes,
    ...mapRoutes,
    ...redirectRoute,
    ...errorRoute,
]

export default routes
