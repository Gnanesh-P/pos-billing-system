export const navigations = [
    {
        name: 'Dashboard',
        path: '/dashboard',
        icon: 'store',
    },
    {
        name: 'Billing',
        path: '/billing',
        icon: 'send',
    },
    {
        name: 'Invoice',
        path: '/customerinvoice',
        icon: 'description',
    },
    {
        name: 'Inventory Management',
        path: '/inventory',
        icon: 'store',
    },

    {
        name: 'Account',
        path: '/account',
        icon: 'store',
    },
    {
        name: 'Customer',
        path: '/customer',
        icon: 'man',
    },
    {
        name: 'Transactions',
        path: '/transactions',
        icon: 'store',
    },
    {
        name: 'Product',
        path: '/product',
        icon: 'store',
    },
    {
        name: 'Category',
        path: '/category',
        icon: 'store',
    },
    {
        name: 'Location',
        path: '/location',
        icon: 'ControlCamera',
    },
    {
        name: 'Warehouse',
        path: '/warehouse',
        icon: 'ControlCamera',
    },
    {
        name: 'Supplier',
        path: '/supplier',
        icon: 'ControlCamera',
    },
    {
        name: 'Units',
        path: '/units',
        icon: 'ControlCamera',
    },
    {
        name: 'Tax',
        path: '/tax',
        icon: 'send',
    },
    {
        name: 'Employee',
        path: '/employee',
        icon: 'send',
    },
]