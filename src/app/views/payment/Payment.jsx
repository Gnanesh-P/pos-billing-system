import React, { useState } from 'react'
import {
    Card,
    Icon,
    IconButton,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Avatar,
    MenuItem, TablePagination,
    Select,
    Button
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import { TextField } from '@material-ui/core'
import Grid from '@material-ui/core/Grid'
// import './style.css'
import TableFooter from '@material-ui/core/TableFooter';
import AddIcon from '@material-ui/icons/Add';
import Autocomplete from '@material-ui/lab/Autocomplete';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import Modal from '@material-ui/core/Modal';
import LockIcon from '@material-ui/icons/Lock';
import AppBar from '@material-ui/core/AppBar';
import Tab from '@material-ui/core/Tab';
import TabContext from '@material-ui/lab/TabContext';
import TabList from '@material-ui/lab/TabList';
import TabPanel from '@material-ui/lab/TabPanel';
import { useHistory } from 'react-router-dom'

const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& small': {
            height: 15,
            width: 50,
            borderRadius: 500,
            boxShadow:
                '0 0 2px 0 rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.24)',
        },
        '& td': {
            borderBottom: 'none',
        },
        '& td:first-child': {
            paddingLeft: '16px !important',
        },
    },
}))

const randomId = () => ['id', Math.floor(Math.random() + 1230384019384)].join("-")

const Payment = () => {

    const history = useHistory()

    const navigateToInvoice = event => {
        history.push("/invoice")
    };

    const classes = useStyles()
    const [open, setOpen] = useState(false);
    const [totalAmount, setTotalAmount] = useState(0)
    const [billingItems, setBillingItems] = useState([{
        imgUrl: null,
        name: null,
        quantity: 0,
        price: 0,
        total: 0,
        id: randomId()
    }])
    const handleOpen = () => {
        setOpen(true);
    };



    const [customerName, setCustomerName] = useState("Mr. ");
    const [mobileNumber, setMobileNumber] = useState("+91 ");

    const [tabChangeValue, setTabChangeValue] = useState(1);
    const handleTabChange = (event, newValue) => {
        setTabChangeValue(newValue);
    };

    const changeStateByName = (index, e, name, newItem) => {
        const { value } = e.target;
        if (name === "name") {
            billingItems[index] = {
                ...newItem, quantity: 1, total: newItem.price,
                id: randomId()
            }
        } else {
            if (name === "total") return;
            if (name === "quantity" || name === "price") {
                if (parseInt(value + "") >= 1)
                    billingItems[index][name] = parseInt(value + "")
            }
            else
                billingItems[index][name] = value

            const currentItem = billingItems[index];
            if (name === "quantity" || name === "price") {
                currentItem.total = currentItem.quantity * currentItem.price;
            }
            billingItems[index] = currentItem;
        }
        setTotalAmount(billingItems.filter(x => x.total > 0).reduce((x, y) => x + y.total, 0))
        setBillingItems([...billingItems])
    }

    const addNewItem = () => {
        setBillingItems([
            ...billingItems,
            {
                imgUrl: null,
                name: null,
                quantity: 0,
                price: 0,
                available: 0,
                id: randomId()
            }]
        )
        setTotalAmount([...billingItems].filter(x => x.total > 0).reduce((x, y) => x + y.total, 0))
    }
    const onDelete = (index, e) => {
        console.log(index);
        billingItems.splice(index, 1)
        setTotalAmount([...billingItems].filter(x => x.total > 0).reduce((x, y) => x + y.total, 0))
        setBillingItems([...billingItems])
    }

    const handleClose = () => {
        setOpen(false);
    }

    return (
        <Card elevation={3} className="pt-5 mb-6" style={{
            padding: "10px", margin: "10px",
            top: `50%`,
            left: `50%`,
            transform: `translate(-50%, -50%)`,
            position: `absolute`,
            padding: `10px`,
            borderRadius: `3px`,
            minWidth: 650,
        }}>
            <Card>
                <div style={{ marginTop: "40px" }} >
                    <TabContext value={tabChangeValue}>
                        <AppBar>
                            <TabList onChange={handleTabChange} centered  >
                                <Tab label="Credit Card" value={1} />
                                <Tab label="GPay" value={2} />
                                <Tab label="Paytm" value={3} />
                            </TabList>
                        </AppBar>

                        <TabPanel value={1} style={{ width: "100%" }}>
                            <div className="master-card" style={{ height: "200px" }} >
                                <div className="flex" style={{ flexDirection: "column", }} >
                                    <h3>Credit Card Details</h3>
                                </div>
                                <div className="flex" style={{ flexDirection: "row" }} >
                                    <label htmlFor="">Credit Card number</label>
                                </div>
                                <div className="flex" style={{ flexDirection: "row", justifyContent: "space-between" }} >
                                    <TextField placeholder="XXXX" max style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    <TextField placeholder="XXXX" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    <TextField placeholder="XXXX" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    <TextField placeholder="XXXX" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                </div>
                                <div className="flex" style={{ flexDirection: "row", marginTop: "10px", justifyContent: "space-between" }} >

                                    <div className="flex" style={{ flexDirection: "column", }} >
                                        <label htmlFor="">Expire Month</label>
                                        <TextField placeholder="MM" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    </div>
                                    <div className="flex" style={{ flexDirection: "column" }} >
                                        <label htmlFor="">Expire Year</label>
                                        <TextField placeholder="YY" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    </div>
                                    <div className="flex" style={{ flexDirection: "column" }} >
                                        <label htmlFor="">Security Code</label>
                                        <TextField placeholder="****" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    </div>
                                </div>
                            </div>
                            <div style={{ display: "flex", justifyContent: "flex-end" }} >
                                <h4>Total To Pay ${totalAmount}</h4>
                            </div>
                        </TabPanel>
                        <TabPanel value={2} style={{ width: "100%" }}>
                            <div style={{ height: "200px" }} >
                                <div className="flex" style={{ flexDirection: "column", }} >
                                    <h3>GPay Details</h3>
                                </div>
                                <div className="flex" style={{ flexDirection: "row" }} >
                                    <label htmlFor="">Please Fill UPI Id</label>
                                </div>
                                <div className="flex" style={{ flexDirection: "row", marginTop: "10px", justifyContent: "space-between" }} >
                                    <TextField type="text" placeholder="****@okicic" style={{ width: "100%" }} variant="outlined" />
                                </div>
                            </div>
                            <div style={{ display: "flex", justifyContent: "flex-end" }} >
                                <h4>Total To Pay ${totalAmount}</h4>
                            </div>
                        </TabPanel>
                        <TabPanel value={3} style={{ width: "100%" }}>
                            <div style={{ height: "200px" }} >
                                <div className="flex" style={{ flexDirection: "column", }} >
                                    <h3>Paytm Details</h3>
                                </div>
                                <div className="flex" style={{ flexDirection: "row" }} >
                                    <label htmlFor="">Please Fill UPI Id</label>
                                </div>
                                <div className="flex" style={{ flexDirection: "row", marginTop: "10px", justifyContent: "space-between" }} >
                                    <TextField type="text" placeholder="****@okicic" style={{ width: "100%" }} variant="outlined" />
                                </div>
                            </div>
                            <div style={{ display: "flex", justifyContent: "flex-end" }} >
                                <h4>Total To Pay ${totalAmount}</h4>
                            </div>
                        </TabPanel>
                    </TabContext>
                </div>
                <div id="payment-gateway-option-footer" style={{ marginTop: "20px" }}>
                    <div style={{ display: "flex", justifyContent: "flex-end", paddingLeft: "20px" }} >
                        <Button variant="contained" color="primary"  >
                            <LockIcon style={{ paddingRight: "5px" }} />
                            PROCESS TO SECURE PAYMENT
                        </Button>
                    </div>
                </div>
            </Card>

        </Card >
    )
}

export default Payment
