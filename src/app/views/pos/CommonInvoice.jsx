import { render } from 'node-sass'
import React, { Component } from 'react'
import Invoice from './InvoicePage'
import Example from './PrintInvoice'

class InvoiceCommon extends React.Component{
    render=()=>{
        return (
            <div>
                <Example/>
                <Invoice></Invoice>
            </div>
        )

    }
}

export default InvoiceCommon;