import ReactToPrint from "react-to-print";
import React, { Component } from 'react'
import Invoice from "./InvoicePage";

class Example extends React.Component {
    render() {
      return (
        <div>
          <ReactToPrint
            trigger={() => <button>Print this out!</button>}
            content={() => this.componentRef}
          />
          <Invoice ref={(el) => (this.componentRef = el)} />
        </div>
      );
    }
  }
  export default Example;