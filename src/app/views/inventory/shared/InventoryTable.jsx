import React, { useState, useEffect } from 'react'
import {
    Card,
    Icon,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Avatar,
    MenuItem, TablePagination,
    Select,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import FormControl from '@material-ui/core/FormControl';
import { connect, useSelector } from 'react-redux';
import { getInventoryBranches, getInventoryItems } from 'app/redux/page/Inventory/InventoryActions';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';
import InventoryWarehouseChildTable from './InventoryWarehouseChildTable';
import UnfoldMoreIcon from '@material-ui/icons/UnfoldMore';
import UnfoldLessIcon from '@material-ui/icons/UnfoldLess';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& small': {
            height: 15,
            width: 50,
            borderRadius: 500,
            boxShadow:
                '0 0 2px 0 rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.24)',
        },
        '& td': {
            borderBottom: 'none',
        }
    },
    expandButton: {
        width: 25
    }
}))

function InventoryTable(props) {
    let { isInventoryListLoading, type, stocks } = props
    stocks = stocks.map(x => ({ ...x, collapse: false }))
    useEffect(() => {
        if (props.loadItem && type) props.loadItem(type);
        if (props.loadBranches) props.loadBranches();
    }, [type])

    const classes = useStyles()

    const [age, setAge] = React.useState('');
    const handleChange = (event) => {
        setAge(event.target.value);
    };

    const collapseButton = (stack) => {
        if (type === 2 && stack.products.length > 1) {
            return stack.collapse ? (<IconButton style={{ padding: 2 }}>
                <UnfoldMoreIcon />
            </IconButton>)
                : (<IconButton style={{ padding: 2 }}>
                    <UnfoldLessIcon />
                </IconButton>)
        } else return <></>
    }
    return (
        <Card elevation={3} className="pt-5 mb-6">
            <div className="flex justify-between items-center px-6 mb-3">
                <span className="card-title">Available Products in {type == 1 ? "Store" : "Warehouse"}</span>
                <div className="flex items-center" >
                    {/* <span class="material-icons" tooltip="Download excel">
                        file_download
                    </span> */}
                    <span className="flex items-center" >
                        <span style={{ paddingRight: "10px" }} >Filter by {type === 1 ? "Store" : "Warehouse"}</span>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <Select value={age} onChange={handleChange}>
                                {props.branches.map(x => <MenuItem value={x.name}>{x.name}</MenuItem>)}
                            </Select>
                        </FormControl>
                    </span>
                </div>
            </div>
            <div className="overflow-auto">
                <Table className={clsx('whitespace-pre min-w-400', classes.productTable)} >
                    <TableHead>
                        <TableRow>
                            <TableCell className="px-6" colSpan={2}>
                                Name
                            </TableCell>
                            <TableCell className="px-6" colSpan={3}>
                                Product Category
                            </TableCell>
                            <TableCell className="px-0" colSpan={2}>
                                Price
                            </TableCell>
                            <TableCell className="px-0" colSpan={2}>
                                Stock Status
                            </TableCell>
                            <TableCell className="px-0" colSpan={type === 2 ? 3 : 1}>
                                Action
                            </TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {isInventoryListLoading &&
                            <TableRow>
                                <TableCell colSpan={type === 2 ? 12 : 10}>
                                    <LinearProgress />
                                </TableCell>
                            </TableRow>}
                        {stocks.map((stack, index) => (
                            <>
                                <TableRow key={index} hover>
                                    <TableCell className="px-0 capitalize" colSpan={2} align="left" >
                                        <div className="flex items-center">
                                            <span className={classes.expandButton}>{collapseButton(stack)}</span>
                                            <p className="m-0">
                                                {stack.product.productName}
                                            </p>
                                        </div>
                                    </TableCell>
                                    <TableCell
                                        className="px-0 capitalize"
                                        colSpan={3}
                                        align="left"
                                    >
                                        <div className="flex items-center">
                                            <p className="m-0 ml-8">
                                                {stack.product.category.categoryName}
                                            </p>
                                        </div>
                                    </TableCell>
                                    <TableCell className="px-0 capitalize" align="left" colSpan={2} >
                                        ${stack.quantity * stack.product.productSellingPrice}
                                    </TableCell>

                                    <TableCell className="px-0" align="left" colSpan={2}    >
                                        {stack.quantity ? (
                                            stack.quantity < 50 ? (
                                                <small className="border-radius-4 bg-secondary text-white px-2 py-2px">
                                                    {stack.quantity} available
                                                </small>
                                            ) : (
                                                <small className="border-radius-4 bg-primary text-white px-2 py-2px">
                                                    in stock
                                                </small>
                                            )
                                        ) : (
                                            <small className="border-radius-4 bg-error text-white px-2 py-2px">
                                                out of stock
                                            </small>
                                        )}
                                    </TableCell>
                                    <TableCell className="px-0 flex" colSpan={type === 2 ? 3 : 2}>
                                        {type === 1 && <Button variant="contained" color="primary">
                                            Request Stock
                                        </Button>}
                                        {type === 2 && stack.products?.length === 1
                                            && <Button variant="contained" color="primary">
                                                Move to Store
                                            </Button>}
                                    </TableCell>
                                </TableRow>

                                {type === 2 && stack.products?.length > 1 &&
                                    <TableRow>
                                        <TableCell className="child-table" colSpan={12}>
                                            <InventoryWarehouseChildTable rows={stack.products}></InventoryWarehouseChildTable>
                                        </TableCell>
                                    </TableRow>}
                            </>

                        ))}
                    </TableBody>

                </Table>
            </div>
        </Card>
    )
}


const mapStateToProps = (state, props) => {
    return {
        stocks: state.inventory.items,
        selectedType: state.inventory.selectedType,
        branches: state.inventory.branches,
        isInventoryListLoading: state.inventory.isInventoryListLoading
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        loadItem: (type) => dispatch(getInventoryItems(type)),
        loadBranches: dispatch(getInventoryBranches()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InventoryTable)