import React, { useState, useEffect } from 'react'
import {
    Card,
    Icon,
    IconButton,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Avatar,
    MenuItem, TablePagination,
    Select,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import FormControl from '@material-ui/core/FormControl';
import { connect, useSelector } from 'react-redux';
import { getInventoryBranches, getInventoryItems } from 'app/redux/page/Inventory/InventoryActions';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& small': {
            height: 15,
            width: 50,
            borderRadius: 500,
            boxShadow:
                '0 0 2px 0 rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.24)',
        },
        '& td': {
            borderBottom: 'none',
        }
    },
}))

function InventoryWarehouseChildTable(props) {
    const classes = useStyles()
    return (
        <Table className={clsx('whitespace-pre min-w-400', classes.productTable)}>
            <TableHead>
                <TableRow>
                    <TableCell className="px-6" colSpan={2}>
                        Date
                    </TableCell>
                    <TableCell className="px-6" colSpan={2}>
                        Quantity
                    </TableCell>
                    <TableCell className="px-6" colSpan={2}>
                        Supplier
                    </TableCell>
                    <TableCell className="px-0" colSpan={2}> </TableCell>
                </TableRow>
            </TableHead>

            <TableBody>
                {props.rows
                    .map((stack, index) => (
                        <TableRow key={index} hover>
                            <TableCell className="px-0 capitalize" colSpan={2} align="left"  >
                                <div className="flex items-center">
                                    <p className="m-0">
                                        {stack.expiryDate || new Date().toLocaleDateString()}
                                    </p>
                                </div>
                            </TableCell>
                            <TableCell className="px-0 capitalize" colSpan={2} align="left" >
                                <div className="flex items-center">
                                    <p className="m-0 ml-8">
                                        {stack.quantity}
                                    </p>
                                </div>
                            </TableCell>
                            <TableCell className="px-0 capitalize" align="left" colSpan={2} >
                                <div className="flex items-center">
                                    <p className="m-0 ml-8">
                                        ${stack.product.productSellingPrice}
                                    </p>
                                </div>
                            </TableCell>


                            <TableCell className="px-0 flex" colSpan={2}>
                                <Button variant="contained" color="primary">
                                    Move to Store
                                </Button>
                            </TableCell>
                        </TableRow>
                    ))}
            </TableBody>
        </Table>
    )
}


const mapStateToProps = (state, props) => {
    return {}
}

const mapDispatchToProps = (dispatch, props) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(InventoryWarehouseChildTable)