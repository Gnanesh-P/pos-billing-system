import React, { useState } from 'react'
import Tab from '@material-ui/core/Tab';
import TabContext from '@material-ui/lab/TabContext';
import TabList from '@material-ui/lab/TabList';
import TabPanel from '@material-ui/lab/TabPanel';

import { Card } from '@material-ui/core'
import InventoryTable from './shared/InventoryTable';

function Inventory(props) {
    const [tabChangeValue, setTabChangeValue] = useState(1);
    const handleTabChange = (event, newValue) => {
        setTabChangeValue(newValue);
    };
    return (
        <Card elevation={3} className="pt-5 mb-6" style={{ padding: "10px", margin: "10px" }}>
            <TabContext value={tabChangeValue}>
                <TabList onChange={handleTabChange}>
                    <Tab label="Store" value={1} />
                    <Tab label="Warehouse" value={2} />
                </TabList>

                <TabPanel value={1} style={{ width: "100%" }}>
                    <InventoryTable type={tabChangeValue}></InventoryTable>
                </TabPanel>
                <TabPanel value={2} style={{ width: "100%" }}>
                    <InventoryTable type={tabChangeValue}></InventoryTable>
                </TabPanel>
            </TabContext>
        </Card>
    )
}

export default Inventory