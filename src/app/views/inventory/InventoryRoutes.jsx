import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const InventoryRoutes = [
    {
        path: '/inventory',
        component: React.lazy(() => import('./Inventory')),
        auth: authRoles.sa,
    }
]

export default InventoryRoutes
