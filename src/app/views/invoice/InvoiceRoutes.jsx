import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const InvoiceRoutes = [
    {
        path: '/invoice',
        component: React.lazy(() => import('./Invoice')),
        auth: authRoles.sa,
    }
]

export default InvoiceRoutes
