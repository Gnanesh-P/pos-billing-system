export const CategoryColumns = [
    { displayName: "Category Code", fieldName: "categoryCode", colspan: 3 },
    { displayName: "Name", fieldName: "categoryName", colspan: 3 },
    { displayName: "Tax", fieldName: "tax", colspan: 3, value: (row) => row.tax && row.tax.taxName ? row.tax.taxName : null },
]