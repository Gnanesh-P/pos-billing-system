import React, { } from 'react'
import { connect } from 'react-redux';
import { getAllCategories } from 'app/views/category/store/CategoryActions';
import CoreGrid from 'app/components/CoreGrid/CoreGrid';
import { CategoryColumns } from './category-columns';

function CategoryTable(props) {
    const onEditHandler = (row, event) => {
        console.log(row, event)
    }
    const onDeleteHandler = (row, event) => {
        console.log(row, event)
    }

    const actionList = [
        { type: "icon", iconName: "edit", handler: onEditHandler },
        { type: "icon", iconName: "delete", handler: onDeleteHandler },
    ]

    return (
        <>
            <CoreGrid
                loading={props.loading}
                columns={CategoryColumns}
                data={props.categories}
                needActions={true}
                actionList={actionList}
                actionColspan={2}
            />
        </>
    )
}

export default connect(
    (state, props) => ({
        categories: state.category.categories,
        loading: state.category.loading
    }),
    (dispatch, props) => ({
        loadCategories: dispatch(getAllCategories())
    })
)(CategoryTable)
