import React from 'react'
import CoreDialog from 'app/components/CoreDialog/CoreDialog'
import { Button } from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { addNewCategory } from '../store/CategoryActions';
import { getAllTaxes } from 'app/views/tax/store/TaxActions';
import CoreZoomSelect from 'app/components/CoreZoomSelect/CoreZoomSelect';

function AddCategory(props) {
    const { taxes, loadTaxes, addNewCategory } = props
    const [open, setOpen] = React.useState(false);

    const taxColumns = [
        { displayName: "Tax Code", fieldName: "taxCode", colspan: 2 },
        { displayName: "Percentage", fieldName: "percentage", colspan: 2 },
        { displayName: "Name", fieldName: "taxName", colspan: 2 },
        { displayName: "Effective Date", fieldName: "effectiveDate", colspan: 3 },
        { displayName: "Expiry Date", fieldName: "expiryDate", colspan: 3 },
    ]

    const onSave = () => {
        setOpen(false);
        addNewCategory(formValue)
    }
    const onCancel = () => {
        setOpen(false);
    }

    React.useEffect(() => {
        setFormValue({});
        setErrorValue({});
        loadTaxes();
    }, [open])

    const [formValue, setFormValue] = React.useState({
        categoryCode: null,
        categoryName: null,
        categoryId: null,
        tax: {
            taxCode: null,
            taxName: null,
            taxId: null,
            effectiveDate: null,
            expiryDate: null,
            percentage: null
        }
    })

    const [errorValue, setErrorValue] = React.useState({})

    const handleOnChange = (fieldName, event) => {
        if (fieldName === "tax") {
            formValue[fieldName] = taxes.find(x => x.taxId == event.taxId)
        }
        else {
            formValue[fieldName] = event.target.value;
        }
        setFormValue({ ...formValue })
    }

    return (
        <>
            <Button variant="contained" onClick={() => setOpen(true)} color="primary">ADD CATEGORY</Button>

            <CoreDialog
                open={open}
                handleClose={onCancel}
                handleCancel={onCancel}
                handleSave={onSave}
                modelTitle="Create Category"
            >
                <Grid container spacing={3}  >
                    <Grid item xs={12} sm={6}>
                        Category Code
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField type="number" value={formValue.categoryCode} onChange={e => handleOnChange("categoryCode", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Name
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.categoryName} onChange={e => handleOnChange("categoryName", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Tax
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <CoreZoomSelect
                            value={formValue?.tax?.taxId}
                            valueField={"taxId"}
                            display={formValue?.tax?.taxName}
                            columns={taxColumns}
                            data={taxes}
                            size={"md"}
                            title="Choose Tax"
                            onSelect={(selectedTax) => handleOnChange("tax", selectedTax)} >
                        </CoreZoomSelect>
                    </Grid>
                </Grid>
            </CoreDialog>
        </>
    )
}

export default connect(
    (state, props) => ({
        taxes: state.tax.taxes
    }),
    (dispatch, props) => ({
        addNewCategory: (x) => dispatch(addNewCategory(x)),
        loadTaxes: () => dispatch(getAllTaxes())
    })
)(AddCategory)