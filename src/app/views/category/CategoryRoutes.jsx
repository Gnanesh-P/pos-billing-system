import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const CategoryRoutes = [
    {
        path: '/category',
        component: React.lazy(() => import('./Category')),
        auth: authRoles.sa,
    }
]

export default CategoryRoutes
