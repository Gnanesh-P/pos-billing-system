
import React from 'react'
import { Card } from '@material-ui/core'
import CategoryTable from './shared/CategoryTable';
import AddCategory from './shared/AddCategory';

export default function Category() {

    return (
        <Card elevation={3} className="pt-5 mb-6" style={{ padding: "10px", margin: "10px" }}>
            <div className="flex justify-between items-center px-6 mb-3">
                <span className="card-title">Category</span>
                <div className="flex items-center" >
                    <span className="flex items-center" >
                        <AddCategory></AddCategory>
                    </span>
                </div>
            </div>

            <CategoryTable />
        </Card>
    )
}
