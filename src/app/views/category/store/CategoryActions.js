import { coreHttpGet, coreHttpPost } from 'axios-utils'
import { utilCopy } from 'utils'

export const categoryActions = {
    getAllCategories: "category/get-all-categories",
    addNewCategory: "category/add-new-category"
}

export const getAllCategories = () => dispatch => {
    dispatch({
        type: categoryActions.getAllCategories,
        payload: {
            items: [],
            loading: true
        }
    })
    coreHttpGet({ url: "/categories" })
        .then(data => {
            dispatch({
                type: categoryActions.getAllCategories,
                payload: {
                    items: utilCopy(data.data),
                    loading: false
                }
            })
        }).catch(x => {
            dispatch({
                type: categoryActions.getAllCategories,
                payload: {
                    items: utilCopy([]),
                    loading: false
                }
            })
        })
}


export const addNewCategory = (category) => dispatch => {
    coreHttpPost({ url: "/categories", data: {...category } }).then(response => {
        dispatch({
            type: categoryActions.addNewCategory,
            payload: response.data
        })
    }).catch(error => {
        console.log(error, )
    })
}