import { categoryActions } from "./CategoryActions"

const initialState = {
    categories: [],
    loading: false
}

const CategoryReducer = function(state = initialState, action) {
    switch (action.type) {
        case categoryActions.getAllCategories:
            return {
                ...state,
                categories: [...action.payload.items],
                loading: action.payload.loading
            }

        case categoryActions.addNewCategory:
            return {
                ...state,
                categories: [...state.categories, action.payload],
            }
        default:
            return {...state }
    }
}

export default CategoryReducer