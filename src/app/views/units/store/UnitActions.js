import { utilCopy } from 'utils'

export const unitActions = {
    getAllUnits: "unit/get-all-units",
    addNewUnit: "unit/add-new-unit"
}

export const getAllUnits = () => dispatch => {
    dispatch({
        type: unitActions.getAllUnits,
        payload: {
            items: [],
            loading: true
        }
    })
    setTimeout(() => {
        dispatch({
            type: unitActions.getAllUnits,
            payload: {
                items: utilCopy([{
                        unitCode: "Dewe",
                        unitName: "Dow",
                        unitId: "wer"
                    },
                    {
                        unitCode: "234",
                        unitName: "Do234w",
                        unitId: "wer34"
                    }
                ]),
                loading: false
            }
        })
    }, 1000);
}


export const addNewUnit = (unit) => dispatch => {
    dispatch({
        type: unitActions.addNewUnit,
        payload: unit
    })
}