import { unitActions } from "./UnitActions"

const initialState = {
    units: [],
    loading: false
}

const UnitReducer = function(state = initialState, action) {
    switch (action.type) {
        case unitActions.getAllUnits:
            return {
                ...state,
                units: [...action.payload.items],
                loading: action.payload.loading
            }

        case unitActions.addNewUnit:
            return {
                ...state,
                units: [...state.units, action.payload],
            }
        default:
            return {...state }
    }
}

export default UnitReducer