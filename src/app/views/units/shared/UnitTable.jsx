import React, { } from 'react'
import { connect } from 'react-redux';
import CoreGrid from 'app/components/CoreGrid/CoreGrid';
import { getAllUnits } from '../store/UnitActions';
import ConfirmationDialog from 'app/views/material-kit/dialog/ConfirmationDialog';
import { UnitColumns } from './UnitColums';

function UnitTable(props) {

    const [confirmation, setConfirmation] = React.useState(false)

    const onEditHandler = (row, event) => {
        console.log(row, event)
    }
    const onDeleteHandler = (row, event) => {
        console.log(row, event)
        setConfirmation(true)
    }
    const onButtonHandler = (row, event) => {
        console.log(row, event);
    }
    const actionList = [
        { type: "icon", iconName: "edit", handler: onEditHandler },
        { type: "icon", iconName: "delete", handler: onDeleteHandler },
    ]

    return (
        <>
            <CoreGrid
                loading={props.loading}
                columns={UnitColumns}
                data={props.units}
                needActions={true}
                actionList={actionList}
            />
        </>
    )
}

export default connect(
    (state, props) => ({
        units: state.unit.units,
        loading: state.unit.loading
    }),
    (dispatch, props) => ({
        loadUnits: dispatch(getAllUnits())
    })
)(UnitTable)
