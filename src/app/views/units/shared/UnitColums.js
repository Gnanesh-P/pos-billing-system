export const UnitColumns = [
    { displayName: "Unit Code", fieldName: "unitCode", colspan: 3 },
    { displayName: "Name", fieldName: "unitName", colspan: 3 },
]