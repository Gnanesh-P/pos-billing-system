import React from 'react'
import CoreDialog from 'app/components/CoreDialog/CoreDialog'
import { Button } from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { addNewUnit } from '../store/UnitActions';

function AddUnit(props) {
    const [open, setOpen] = React.useState(false);

    const onSave = () => {
        setOpen(false);
        props.addNewUnit(formValue)
    }
    const onCancel = () => {
        setOpen(false);
    }

    React.useEffect(() => {
        setFormValue({})
        setErrorValue({})
    }, [open])

    const [formValue, setFormValue] = React.useState({
        unitCode: null,
        unitName: null,
        unitId: null,
    })

    const [errorValue, setErrorValue] = React.useState({})

    const handleOnChange = (fieldName, event) => {
        formValue[fieldName] = event.target.value;
        setFormValue({ ...formValue })
    }

    return (
        <>
            <Button variant="contained" onClick={() => setOpen(true)} color="primary">ADD CATEGORY</Button>

            <CoreDialog
                open={open}
                handleClose={onCancel}
                handleCancel={onCancel}
                handleSave={onSave}
                modelTitle="Create Unit"
            >
                <Grid container spacing={3}  >
                    <Grid item xs={12} sm={6}>
                        Unit Code
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.unitCode} onChange={e => handleOnChange("unitCode", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Name
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.unitName} onChange={e => handleOnChange("unitName", e)} variant="outlined" />
                    </Grid>
                </Grid>
            </CoreDialog>
        </>
    )
}

export default connect(
    (state, props) => ({}),
    (dispatch, props) => ({
        addNewUnit: (x) => dispatch(addNewUnit(x))
    })
)(AddUnit)