import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const UnitsRoutes = [
    {
        path: '/units',
        component: React.lazy(() => import('./Units')),
        auth: authRoles.sa,
    }
]

export default UnitsRoutes
