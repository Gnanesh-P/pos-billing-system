import axios from 'axios'
import { coreHttpGet, coreHttpPost } from 'axios-utils';

export const warehouseActions = {
    getAllWarehouse: "warehouse/get-all-warehouse",
}

export const getAllWarehouse = () => dispatch => {
    dispatch({
        type: warehouseActions.getAllWarehouse,
        payload: {
            loading: true,
            items: []
        }
    })

    coreHttpGet({ url: "/warehouse" }).then(data => {
        dispatch({
            type: warehouseActions.getAllWarehouse,
            payload: {
                loading: false,
                items: data.data
            }
        })
    }).catch(err => {
        dispatch({
            type: warehouseActions.getAllWarehouse,
            payload: {
                loading: false,
                items: []
            }
        })
    })
}

export const addNewWarehouse = (warehouse) => dispatch => {
    coreHttpPost({ url: "/warehouse", data: warehouse }).then(response => {
        dispatch(getAllWarehouse())
    }).catch(err => { console.log(err) })
}