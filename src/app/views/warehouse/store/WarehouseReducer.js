import { warehouseActions } from "./WarehouseActions"

const initialState = {
    warehouses: [],
    loading: false
}

const WarehouseReducer = function(state = initialState, action) {
    switch (action.type) {
        case warehouseActions.getAllWarehouse:
            return {
                ...state,
                warehouses: [...action.payload.items],
                loading: action.payload.loading
            }
        default:
            return {...state }
    }
}

export default WarehouseReducer