import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const WarehouseRoutes = [
    {
        path: '/warehouse',
        component: React.lazy(() => import('./Warehouse')),
        auth: authRoles.sa,
    },
    {
        path: '/warehouse-location/:warehouseId',
        component: React.lazy(() => import('../location/Location')),
        auth: authRoles.sa,
    }
]

export default WarehouseRoutes
