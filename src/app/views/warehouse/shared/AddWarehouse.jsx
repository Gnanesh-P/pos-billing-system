import React from 'react'
import CoreDialog from 'app/components/CoreDialog/CoreDialog'
import { Button } from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { addNewWarehouse } from '../store/WarehouseActions';
import { getAllEmployees } from 'app/views/employee/store/EmployeeActions';
import CoreZoomSelect from 'app/components/CoreZoomSelect/CoreZoomSelect';

function AddWarehouse({ addNewWarehouse, employees, loadEmployees }) {

    const employeeColumns = [
        { displayName: "Employee Code", fieldName: "employeeCode", colspan: 3 },
        { displayName: "Name", fieldName: "employeeName", colspan: 3 },
        { displayName: "Contact Number", fieldName: "contactNumber", colspan: 3 },
    ]

    const [open, setOpen] = React.useState(false);

    const onSave = () => {
        setOpen(false);
        addNewWarehouse(formValue)
    }
    const onCancel = () => {
        setOpen(false);
    }

    const tempFormValue = {
        warehouseId: null,
        warehouseAddress: null,
        warehouseCode: null,
        city: null,
        state: null,
        country: null,
        employee: {
            employeeCode: null,
            employeeName: null,
            employeeId: null,
            contactNumber: null
        }
    }

    // onMount
    React.useEffect(() => {
        setFormValue({ ...tempFormValue })
        setErrorValue({});
        loadEmployees();
    }, [])

    const [formValue, setFormValue] = React.useState({ ...tempFormValue })
    const [errorValue, setErrorValue] = React.useState({})

    const handleOnChange = (fieldName, event) => {
        if (fieldName === "employee") {
            formValue[fieldName] = employees.find(x => x.employeeId == event.employeeId) || {}
        } else {
            formValue[fieldName] = event.target.value;
        }
        setFormValue({ ...formValue })
    }

    return (
        <>
            <Button variant="contained" onClick={() => setOpen(true)} color="primary">ADD WAREHOUSE</Button>

            <CoreDialog
                open={open}
                handleClose={onCancel}
                handleCancel={onCancel}
                handleSave={onSave}
                modelTitle="Create Warehouse"
            >
                <Grid container spacing={3}  >
                    <Grid item xs={12} sm={6}>
                        Warehouse Id
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField type="number" value={formValue.warehouseId} onChange={e => handleOnChange("warehouseId", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Warehouse Code
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.warehouseCode} onChange={e => handleOnChange("warehouseCode", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        City
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.city} onChange={e => handleOnChange("city", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        State
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.state} onChange={e => handleOnChange("state", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Country
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.country} onChange={e => handleOnChange("country", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Address
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.warehouseAddress} onChange={e => handleOnChange("warehouseAddress", e)} variant="outlined" />
                    </Grid>


                    <Grid item xs={12} sm={6}>
                        Employee
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <CoreZoomSelect
                            value={formValue?.employee?.employeeId}
                            valueField={"employeeId"}
                            display={formValue?.employee?.employeeName}
                            columns={employeeColumns}
                            data={employees}
                            size={"md"}
                            title="Choose Employee"
                            onSelect={(selectedEmployee) => handleOnChange("employee", selectedEmployee)} >
                        </CoreZoomSelect>
                    </Grid>

                </Grid>
            </CoreDialog>
        </>
    )
}

export default connect(
    (state, props) => ({
        employees: state.employee.employees
    }),
    (dispatch, props) => ({
        addNewWarehouse: (warehouse) => dispatch(addNewWarehouse(warehouse)),
        loadEmployees: () => dispatch(getAllEmployees())
    })
)(AddWarehouse)