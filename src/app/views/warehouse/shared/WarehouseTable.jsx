import React from 'react'
import { connect } from 'react-redux';
import CoreGrid from 'app/components/CoreGrid/CoreGrid';
import { getAllWarehouse } from '../store/WarehouseActions';
import AddLocation from 'app/views/location/shared/AddLocation';
import { useHistory } from 'react-router-dom';
import { WarehouseColumns } from './WarehouseColumns';

function WarehouseTable({ warehouses, loading, loadWarehouse }) {
    const history = useHistory()
    const onClickLink = (row) => {
        history.replace("/warehouse-location/" + row.warehouseId)
    }


    React.useEffect(() => { loadWarehouse() }, [])

    const onEditHandler = (row, event) => {
        console.log(row, event)
    }
    const onDeleteHandler = (row, event) => {
        console.log(row, event)
    }
    const [openLocationCreate, setOpenLocationCreate] = React.useState(false)
    const onOpenLocation = (row, event) => {
        setOpenLocationCreate(false);
        setTimeout(() => { setOpenLocationCreate(true); }, 0);
    }
    const actionList = [
        { type: "icon", iconName: "edit", handler: onEditHandler },
        { type: "icon", iconName: "delete", handler: onDeleteHandler },
        { type: "button", buttonName: "Add Location", handler: onOpenLocation },
    ]

    return (
        <>
            <AddLocation fromWarehouse={true} fromWarehouseOpen={openLocationCreate}></AddLocation>
            <CoreGrid
                loading={loading}
                columns={WarehouseColumns({ onWarehouseClick: onClickLink })}
                data={warehouses}
                needActions={true}
                actionList={actionList}
                actionColspan={4}
            />
        </>
    )
}

export default connect(
    (state, props) => ({
        warehouses: state.warehouse.warehouses,
        loading: state.warehouse.loading
    }),
    (dispatch, props) => ({
        loadWarehouse: () => dispatch(getAllWarehouse())
    })
)(WarehouseTable)
