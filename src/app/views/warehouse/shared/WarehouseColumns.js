export const WarehouseColumns = ({ onWarehouseClick }) => [
    { displayName: "Warehouse Id", fieldName: "warehouseId", colspan: 2, isLink: true, onClickLink: onWarehouseClick },
    { displayName: "Warehouse Code", fieldName: "warehouseCode", colspan: 2, isLink: true, onClickLink: onWarehouseClick },
    { displayName: "Address", fieldName: "warehouseAddress", colspan: 3 },
    { displayName: "City", fieldName: "city", colspan: 2 },
    { displayName: "State", fieldName: "state", colspan: 2 },
    { displayName: "Country", fieldName: "Country", colspan: 2 },
]