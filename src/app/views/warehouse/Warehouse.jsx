
import React from 'react'
import { Button, Card } from '@material-ui/core'
import AddWarehouse from './shared/AddWarehouse'
import WarehouseTable from './shared/WarehouseTable'

export default function warehouse() {
    return (
        <Card elevation={3} className="pt-5 mb-6" style={{ padding: "10px", margin: "10px" }}>

            <div className="flex justify-between items-center px-6 mb-3">
                <span className="card-title">Warehouse</span>
                <div className="flex items-center" >
                    <span className="flex items-center" >
                        <AddWarehouse></AddWarehouse>
                    </span>
                </div>
            </div>

            <WarehouseTable />
        </Card>
    )
}
