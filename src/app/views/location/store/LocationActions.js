import axios from 'axios'
import { coreHttpGet, coreHttpPost } from 'axios-utils';

export const locationActions = {
    getAllLocation: "location/get-all-location",
}

export const getAllLocation = () => dispatch => {
    dispatch({
        type: locationActions.getAllLocation,
        payload: {
            loading: true,
            items: []
        }
    })

    coreHttpGet({ url: "/location" }).then(data => {
        dispatch({
            type: locationActions.getAllLocation,
            payload: {
                loading: false,
                items: data.data
            }
        })
    }).catch(err => {
        dispatch({
            type: locationActions.getAllLocation,
            payload: {
                loading: false,
                items: []
            }
        })
    })
}

export const addNewLocation = (location) => dispatch => {
    coreHttpPost({ url: "/location", data: location }).then(response => {
        dispatch(getAllLocation())
    }).catch(err => { console.log(err) })
}