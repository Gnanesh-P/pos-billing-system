import { locationActions } from "./LocationActions"

const initialState = {
    locations: [],
    loading: false
}

const LocationReducer = function(state = initialState, action) {
    switch (action.type) {
        case locationActions.getAllLocation:
            return {
                ...state,
                locations: [...action.payload.items],
                loading: action.payload.loading
            }
        default:
            return {...state }
    }
}

export default LocationReducer