
import React from 'react'
import { Card } from '@material-ui/core'
import AddLocation from './shared/AddLocation'
import LocationTable from './shared/LocationTable'

export default function Location() {
    return (
        <Card elevation={3} className="pt-5 mb-6" style={{ padding: "10px", margin: "10px" }}>

            <div className="flex justify-between items-center px-6 mb-3">
                <span className="card-title">Location</span>
                <div className="flex items-center" >
                    <span className="flex items-center" >
                        <AddLocation></AddLocation>
                    </span>
                </div>
            </div>

            <LocationTable />
        </Card>
    )
}
