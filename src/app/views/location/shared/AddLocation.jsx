import React from 'react'
import CoreDialog from 'app/components/CoreDialog/CoreDialog'
import { Button } from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { addNewLocation } from '../store/LocationActions';
import { getAllWarehouse } from 'app/views/warehouse/store/WarehouseActions'
import CoreZoomSelect from 'app/components/CoreZoomSelect/CoreZoomSelect';

function AddLocation({
    warehouses,
    addNewLocation,
    loadWarehouse,

    fromWarehouseOpen,
    fromWarehouse
}) {

    const [open, setOpen] = React.useState(false);

    React.useEffect(() => { setOpen(fromWarehouseOpen) }, [fromWarehouseOpen])
    
    const warehouseColumns = [
        { displayName: "Warehouse Id", fieldName: "warehouseId", colspan: 2, },
        { displayName: "Warehouse Code", fieldName: "warehouseCode", colspan: 2 },
        { displayName: "Address", fieldName: "warehouseAddress", colspan: 3 },
        { displayName: "City", fieldName: "city", colspan: 3 },
        { displayName: "State", fieldName: "state", colspan: 3 },
        { displayName: "Country", fieldName: "Country", colspan: 3 },
    ]

    const onSave = () => {
        setOpen(false);
        addNewLocation(formValue)
    }
    const onCancel = () => {
        setOpen(false);
    }

    const tempFormValue = {
        warehouse: {
            warehouseId: null,
            warehouseCode: null,
            warehouseLocation: null,
            warehouseAddress: null,
            managerName: null,
            managerContactNumber: null
        },
        locationCode: null,
        locationDescription: null,
        row: null,
        rack: null,
        column: null
    }

    React.useEffect(() => {
        setFormValue({ ...tempFormValue })
        setErrorValue({})
        loadWarehouse();
    }, [])

    const [formValue, setFormValue] = React.useState({ ...tempFormValue })
    const [errorValue, setErrorValue] = React.useState({})

    const handleOnChange = (fieldName, event) => {
        if (fieldName === "warehouse") {
            formValue[fieldName] = warehouses.find(x => x.warehouseId === event.warehouseId) || {}
        } else {
            formValue[fieldName] = event.target.value;
        }

        setFormValue({ ...formValue })
    }

    return (
        <>
            {!fromWarehouse && <Button variant="contained" onClick={() => setOpen(true)} color="primary">ADD LOCATION</Button>}

            <CoreDialog
                open={open}
                handleClose={onCancel}
                handleCancel={onCancel}
                handleSave={onSave}
                modelTitle="Create Location"
            >
                <Grid container spacing={3}  >
                    <Grid item xs={12} sm={6}>
                        Location Code
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.locationCode} onChange={e => handleOnChange("locationCode", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Description
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.locationDescription} onChange={e => handleOnChange("locationDescription", e)} variant="outlined" />
                    </Grid>


                    <Grid item xs={12} sm={6}>
                        Rack
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.rack} onChange={e => handleOnChange("rack", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Row
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.row} onChange={e => handleOnChange("row", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Column
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.column} onChange={e => handleOnChange("column", e)} variant="outlined" />
                    </Grid>


                    <Grid item xs={12} sm={6}>
                        Warehouse
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <CoreZoomSelect
                            value={formValue?.warehouse?.warehouseId}
                            valueField={"warehouseId"}
                            display={formValue?.warehouse?.warehouseCode}
                            columns={warehouseColumns}
                            data={warehouses}
                            size={"md"}
                            title="Choose Warehouse"
                            onSelect={(selectedWH) => handleOnChange("warehouse", selectedWH)} >
                        </CoreZoomSelect>
                    </Grid>

                </Grid>
            </CoreDialog>
        </>
    )
}

export default connect(
    (state, props) => {
        console.log(props)
        return ({
            warehouses: state.warehouse.warehouses,
            ...props
        })
    },
    (dispatch, props) => ({
        addNewLocation: (supplier) => dispatch(addNewLocation(supplier)),
        loadWarehouse: () => dispatch(getAllWarehouse())
    })
)(AddLocation)