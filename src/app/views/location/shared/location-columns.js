export const LocationColumns = [
    { displayName: "Location Code", fieldName: "locationCode", colspan: 2 },
    { displayName: "Description", fieldName: "locationDescription", colspan: 3 },
    { displayName: "Rack", fieldName: "rack", colspan: 2 },
    { displayName: "Row", fieldName: "row", colspan: 2 },
    { displayName: "column", fieldName: "column", colspan: 2 },
    { displayName: "Warehouse", fieldName: "warehouse", colspan: 2, value: (row) => row.warehouse && row.warehouse.warehouseLocation ? row.warehouse.warehouseLocation : null },
]
