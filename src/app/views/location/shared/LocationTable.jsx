import React, { } from 'react'
import { connect } from 'react-redux';
import CoreGrid from 'app/components/CoreGrid/CoreGrid';
import { getAllLocation } from '../store/LocationActions';
import { LocationColumns } from './location-columns';

function LocationTable({ locations, loading }) {
    const onEditHandler = (row, event) => {
        console.log(row, event)
    }
    const onDeleteHandler = (row, event) => {
        console.log(row, event)
    }
    const onButtonHandler = (row, event) => {
        console.log(row, event);
    }
    const actionList = [
        { type: "icon", iconName: "edit", handler: onEditHandler },
        { type: "icon", iconName: "delete", handler: onDeleteHandler },
    ]

    return (
        <>
            <CoreGrid
                loading={loading}
                columns={LocationColumns}
                data={locations}
                needActions={true}
                actionList={actionList}
                actionColspan={2}
            />
        </>
    )
}

export default connect(
    (state, props) => ({
        locations: state.location.locations,
        loading: state.location.loading
    }),
    (dispatch, props) => ({
        loadLocation: dispatch(getAllLocation())
    })
)(LocationTable)
