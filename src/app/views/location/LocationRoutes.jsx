import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const LocationRoutes = [
    {
        path: '/location',
        component: React.lazy(() => import('./Location')),
        auth: authRoles.sa,
    }
]

export default LocationRoutes
