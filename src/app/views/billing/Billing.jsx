import React, { Fragment } from 'react'
import { Grid, Button, Card } from '@material-ui/core'
import BillingTable from './shared/BillingTable'
import { useTheme } from '@material-ui/styles'
import { makeStyles } from '@material-ui/core/styles';
import BillingHeader from './shared/BillingHeader';
import './shared/style.css'

const Billing = () => {
    const theme = useTheme()

    return (
        <Fragment>
            <div className="analytics m-tb-5">
                <Grid container spacing={3}>
                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <Card id="billing-page" elevation={3} className="pt-5 mb-6" style={{ padding: "10px", margin: "10px" }}>
                            <div className="overflow-auto">
                                <BillingHeader />
                                <BillingTable />
                            </div >
                        </Card >
                    </Grid>
                </Grid>
            </div>
        </Fragment>
    )
}

export default Billing
