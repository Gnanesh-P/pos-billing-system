import React, { useState } from 'react'
import {
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Button
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import { TextField } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import { useHistory } from 'react-router-dom'

const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& small': {
            height: 15,
            width: 50,
            borderRadius: 500,
            boxShadow:
                '0 0 2px 0 rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.24)',
        },
        '& td': {
            borderBottom: 'none',
        },
    },
    nameInput: {
        padding: 0,
        backgroundColor: "red"
    }
}))

const randomId = () => ['id', Math.floor(Math.random() + 1230384019384)].join("-")

const BillingTable = () => {
    const history = useHistory()

    const navigateToInvoice = event => {
        history.push("/invoice")
    };

    const classes = useStyles()

    const [totalAmount, setTotalAmount] = useState(0)
    const [billingItems, setBillingItems] = useState([{
        imgUrl: null,
        name: null,
        quantity: 0,
        price: 0,
        total: 0,
        id: randomId()
    }])

    const changeStateByName = (index, e, name, newItem) => {
        const { value } = e.target;
        if (name === "name") {
            billingItems[index] = {
                ...newItem, quantity: 1, total: newItem.price,
                id: randomId()
            }
        } else {
            if (name === "total") return;
            if (name === "quantity" || name === "price") {
                if (parseInt(value + "") >= 1)
                    billingItems[index][name] = parseInt(value + "")
            }
            else
                billingItems[index][name] = value

            const currentItem = billingItems[index];
            if (name === "quantity" || name === "price") {
                currentItem.total = currentItem.quantity * currentItem.price;
            }
            billingItems[index] = currentItem;
        }
        setTotalAmount(billingItems.filter(x => x.total > 0).reduce((x, y) => x + y.total, 0))
        setBillingItems([...billingItems])
    }

    const addNewItem = () => {
        setBillingItems([
            ...billingItems,
            {
                imgUrl: null,
                name: null,
                quantity: 0,
                price: 0,
                available: 0,
                id: randomId()
            }]
        )
        setTotalAmount([...billingItems].filter(x => x.total > 0).reduce((x, y) => x + y.total, 0))
    }
    const onDelete = (index, e) => {
        console.log(index);
        billingItems.splice(index, 1)
        setTotalAmount([...billingItems].filter(x => x.total > 0).reduce((x, y) => x + y.total, 0))
        setBillingItems([...billingItems])
    }

    return (
        <Table className={clsx('whitespace-pre min-w-400', classes.productTable)} >
            <TableHead>
                <TableRow>
                    <TableCell className="px-0" colSpan={6}>
                        Name
                    </TableCell>
                    <TableCell className="px-0" colSpan={1}>
                        Quantity
                    </TableCell>
                    <TableCell className="px-0" colSpan={1}>
                        Price
                    </TableCell>
                    <TableCell className="px-0" colSpan={1}>
                        Amount
                    </TableCell>
                    <TableCell className="px-0" colSpan={1}>
                        Remove
                    </TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {billingItems.map((product, index) => (
                    <TableRow key={product.id} >
                        <TableCell colSpan={6} align="left">
                            <div style={{ display: "flex", width: "100%" }} >
                                <Autocomplete
                                    className={classes.nameInput}
                                    options={productList}
                                    onChange={(e, newItem) => { changeStateByName(index, e, "name", newItem) }}
                                    style={{ width: "100%" }}
                                    getOptionLabel={(option) => option.name}
                                    renderInput={(params) => <TextField {...params} variant="outlined" />}
                                />
                            </div>
                        </TableCell>
                        <TableCell
                            className="px-0 capitalize"
                            colSpan={1}
                            align="left"
                        >
                            <TextField value={product.quantity} variant="outlined"
                                style={{ width: 100 }}
                                type="number"
                                onChange={(e) => { changeStateByName(index, e, "quantity") }} />
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left" colSpan={1} >
                            <TextField style={{ width: 100 }} value={product.price} type="number" variant="outlined"
                                onChange={(e) => { changeStateByName(index, e, "price") }} />
                        </TableCell>


                        <TableCell className="px-0" colSpan={1}>
                            <TextField style={{ width: 100 }} type="number" value={product.total} onChange={(e) => { changeStateByName(index, e, "total") }} variant="outlined" />
                        </TableCell>
                        <TableCell className="px-0" colSpan={1}>
                            <div style={{ cursor: "pointer", display: "flex", padding: "0 15px" }}>
                                <DeleteOutlineIcon onClick={(e) => { onDelete(index, e) }} />
                            </div>
                        </TableCell>
                    </TableRow>
                ))}
                <TableRow>
                    <div style={{ display: "flex", flexDirection: "column" }}>
                        <div style={{ paddingLeft: "60px" }}>
                            <Button variant="contained" color="primary" onClick={addNewItem}>
                                {/* <AddIcon /> */}
                                ADD ITEM
                            </Button>
                        </div>
                    </div>
                </TableRow>
                <TableRow>
                    <TableCell colSpan={7}></TableCell>
                    <TableCell colSpan={3}>
                        <table>
                            <tr>
                                <td>
                                    <h4>Total Amount</h4>
                                </td>
                                <td>
                                    <h4>: ${totalAmount}</h4>
                                </td>
                            </tr>
                        </table>
                    </TableCell>
                </TableRow>

                <TableRow>
                    <TableCell colSpan={7}> </TableCell>
                    <TableCell colSpan={3}>
                        <div style={{ display: "flex", justifyContent: "flex-end", paddingLeft: "20px", marginRight: "55px" }} >
                            <Button variant="contained" onClick={navigateToInvoice} color="primary"  >
                                <MonetizationOnIcon style={{ paddingRight: "5px" }} />
                                GENERATE BILL
                            </Button>
                        </div>
                    </TableCell>
                </TableRow>
            </TableBody>
        </Table>
    )
}

export const productList = [
    {
        imgUrl: '/assets/images/products/headphone-2.jpg',
        name: 'Eggs',
        price: 5
    },
    {
        imgUrl: '/assets/images/products/iphone-2.jpg',
        name: 'Butter',
        price: 30,
    },
    {
        imgUrl: '/assets/images/Items/cooking-oil.jpg',
        name: 'Cooking oil',
        price: 75,
    },
    {
        imgUrl: '/assets/images/Items/rin-washing-soap.png',
        name: 'Soup',
        price: 35,
    },
    {
        imgUrl: '/assets/images/Items/honey.jpg',
        name: 'Honey',
        price: 248,
    },
    {
        imgUrl: '/assets/images/Items/sugar.jpg',
        name: 'Sugar',
        price: 300,
    },
]

export default BillingTable
