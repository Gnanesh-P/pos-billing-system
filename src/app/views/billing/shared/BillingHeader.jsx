import React, { useState } from 'react'
import { TextField } from '@material-ui/core'
import Grid from '@material-ui/core/Grid'


export default function BillingHeader() {

    const [customerName, setCustomerName] = useState("Mr. ");
    const [mobileNumber, setMobileNumber] = useState("+91 ");

    return (
        <>
            <Grid item lg={12} md={12} sm={12} xs={12} style={{ display: "flex", justifyContent: "center" }}>
                <h3 style={{ paddingLeft: "60px", color: "#1976d2" }} >Billing</h3>
            </Grid>

            <Grid item lg={12} md={12} sm={12} xs={12}>
                <div style={{ display: 'flex', justifyContent: "space-between", alignItems: "center", }} >
                    <div className="form-control" >
                        <label>Mobile Number </label>
                        <TextField value={mobileNumber} onChange={(e) => setMobileNumber(e.target.value)} style={{ marginBottom: "10px", width: "350px", marginRight: "55px" }} variant="outlined" />
                    </div>

                    <div className="form-control" >
                        <label>Customer Name </label>
                        <TextField value={customerName} onChange={(e) => setCustomerName(e.target.value)} variant="outlined" style={{ marginBottom: "10px", width: "350px" }} />
                    </div>
                </div>
            </Grid>
        </>
    )
}
