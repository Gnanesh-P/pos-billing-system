import React, { useState } from 'react'
import Modal from '@material-ui/core/Modal';
import LockIcon from '@material-ui/icons/Lock';
import AppBar from '@material-ui/core/AppBar';
import Tab from '@material-ui/core/Tab';
import TabContext from '@material-ui/lab/TabContext';
import TabList from '@material-ui/lab/TabList';
import TabPanel from '@material-ui/lab/TabPanel';
import {
    Card,
    Button
} from '@material-ui/core'
import { TextField } from '@material-ui/core'

export default function Payment(props) {
    const totalAmount = 324234;
    const [open, setOpen] = useState(false);
    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };


    const [tabChangeValue, setTabChangeValue] = useState(1);
    const handleTabChange = (event, newValue) => {
        setTabChangeValue(newValue);
    };

    return (
        <Modal
            open={open}
            onClose={handleClose}
            style={{ position: "absolute" }}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            <Card style={{
                top: `50%`,
                left: `50%`,
                transform: `translate(-50%, -50%)`,
                position: `absolute`,
                padding: `10px`,
                borderRadius: `3px`,
                minWidth: 650,
            }} >
                <div style={{ marginTop: "40px" }} >
                    <TabContext value={tabChangeValue}>
                        <AppBar>
                            <TabList onChange={handleTabChange} centered  >
                                <Tab label="Credit Card" value={1} />
                                <Tab label="GPay" value={2} />
                                <Tab label="Paytm" value={3} />
                            </TabList>
                        </AppBar>

                        <TabPanel value={1} style={{ width: "100%" }}>
                            <div className="master-card" style={{ height: "200px" }} >
                                <div className="flex" style={{ flexDirection: "column", }} >
                                    <h3>Credit Card Details</h3>
                                </div>
                                <div className="flex" style={{ flexDirection: "row" }} >
                                    <label htmlFor="">Credit Card number</label>
                                </div>
                                <div className="flex" style={{ flexDirection: "row", justifyContent: "space-between" }} >
                                    <TextField placeholder="XXXX" max style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    <TextField placeholder="XXXX" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    <TextField placeholder="XXXX" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    <TextField placeholder="XXXX" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                </div>
                                <div className="flex" style={{ flexDirection: "row", marginTop: "10px", justifyContent: "space-between" }} >

                                    <div className="flex" style={{ flexDirection: "column", }} >
                                        <label htmlFor="">Expire Month</label>
                                        <TextField placeholder="MM" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    </div>
                                    <div className="flex" style={{ flexDirection: "column" }} >
                                        <label htmlFor="">Expire Year</label>
                                        <TextField placeholder="YY" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    </div>
                                    <div className="flex" style={{ flexDirection: "column" }} >
                                        <label htmlFor="">Security Code</label>
                                        <TextField placeholder="****" style={{ width: 135, marginRight: "5px" }} type="number" variant="outlined" />
                                    </div>
                                </div>
                            </div>
                            <div style={{ display: "flex", justifyContent: "flex-end" }} >
                                <h4>Total To Pay ${totalAmount}</h4>
                            </div>
                        </TabPanel>
                        <TabPanel value={2} style={{ width: "100%" }}>
                            <div style={{ height: "200px" }} >
                                <div className="flex" style={{ flexDirection: "column", }} >
                                    <h3>GPay Details</h3>
                                </div>
                                <div className="flex" style={{ flexDirection: "row" }} >
                                    <label htmlFor="">Please Fill UPI Id</label>
                                </div>
                                <div className="flex" style={{ flexDirection: "row", marginTop: "10px", justifyContent: "space-between" }} >
                                    <TextField type="text" placeholder="****@okicic" style={{ width: "100%" }} variant="outlined" />
                                </div>
                            </div>
                            <div style={{ display: "flex", justifyContent: "flex-end" }} >
                                <h4>Total To Pay ${totalAmount}</h4>
                            </div>
                        </TabPanel>
                        <TabPanel value={3} style={{ width: "100%" }}>
                            <div style={{ height: "200px" }} >
                                <div className="flex" style={{ flexDirection: "column", }} >
                                    <h3>Paytm Details</h3>
                                </div>
                                <div className="flex" style={{ flexDirection: "row" }} >
                                    <label htmlFor="">Please Fill UPI Id</label>
                                </div>
                                <div className="flex" style={{ flexDirection: "row", marginTop: "10px", justifyContent: "space-between" }} >
                                    <TextField type="text" placeholder="****@okicic" style={{ width: "100%" }} variant="outlined" />
                                </div>
                            </div>
                            <div style={{ display: "flex", justifyContent: "flex-end" }} >
                                <h4>Total To Pay ${totalAmount}</h4>
                            </div>
                        </TabPanel>
                    </TabContext>
                </div>
                <div id="payment-gateway-option-footer" style={{ marginTop: "20px" }}>
                    <div style={{ display: "flex", justifyContent: "flex-end", paddingLeft: "20px" }} >
                        <Button variant="contained" color="primary"  >
                            <LockIcon style={{ paddingRight: "5px" }} />
                            PROCESS TO SECURE PAYMENT
                        </Button>
                    </div>
                </div>
            </Card>
        </Modal>
    )
}



const paymentMethods = [
    {
        name: "GPay",
        imageUrl: "/assets/images/payments/gpay.png"
    },
    {
        name: "Paytm",
        imageUrl: "/assets/images/payments/paytm.png"
    },
    {
        name: "Master Card",
        imageUrl: "/assets/images/payments/master-card.png"
    },
    {
        name: "Phone Pe",
        imageUrl: "/assets/images/payments/phonepe.png"
    }
]