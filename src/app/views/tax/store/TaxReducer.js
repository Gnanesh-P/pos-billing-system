import { taxActions } from "./TaxActions"

const initialState = {
    taxes: [],
    loading: false
}

const TaxReducer = function (state = initialState, action) {
    switch (action.type) {
        case taxActions.getAllTaxes:
            return {
                ...state,
                taxes: [...action.payload.items],
                loading: action.payload.loading
            }

        case taxActions.addNewTax:
            return {
                ...state,
                taxes: [...state.taxes, action.payload],
            }
        default:
            return { ...state }
    }
}

export default TaxReducer