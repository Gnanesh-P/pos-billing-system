import { utilCopy } from 'utils'

export const taxActions = {
    getAllTaxes: "tax/get-all-taxes",
    addNewTax: "tax/add-new-tax"
}

export const getAllTaxes = () => dispatch => {
    dispatch({
        type: taxActions.getAllTaxes,
        payload: {
            items: [],
            loading: true
        }
    })
    setTimeout(() => {
        dispatch({
            type: taxActions.getAllTaxes,
            payload: {
                items: utilCopy([{
                    taxCode: "Dewe",
                    taxName: "Dow",
                    taxId: "wer",
                    effectiveDate: new Date("2021-10-22"),
                    expiryDate: new Date(),
                    percentage: 2
                },
                {
                    taxCode: "Apple",
                    taxName: "Apple",
                    taxId: "wers",
                    effectiveDate: new Date("2021-10-22"),
                    expiryDate: new Date(),
                    percentage: 2
                },
                {
                    taxCode: "Junir@#@",
                    taxName: "Junir",
                    taxId: "Junir",
                    effectiveDate: new Date("2021-10-22"),
                    expiryDate: new Date(),
                    percentage: 2
                },]),
                loading: false
            }
        })
    }, 1000);
}


export const addNewTax = (tax) => dispatch => {
    dispatch({
        type: taxActions.addNewTax,
        payload: tax
    })
}