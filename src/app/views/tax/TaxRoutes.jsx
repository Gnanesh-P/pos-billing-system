import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const TaxRoutes = [
    {
        path: '/tax',
        component: React.lazy(() => import('./Tax')),
        auth: authRoles.sa,
    }
]

export default TaxRoutes
