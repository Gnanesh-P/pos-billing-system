import React, { } from 'react'
import { connect } from 'react-redux';
import CoreGrid from 'app/components/CoreGrid/CoreGrid';
import { getAllTaxes } from '../store/TaxActions';

function TaxTable(props) {

    const [confirmation, setConfirmation] = React.useState(false)

    const columns = [
        { displayName: "Tax Code", fieldName: "taxCode", colspan: 2 },
        { displayName: "Percentage", fieldName: "percentage", colspan: 2 },
        { displayName: "Name", fieldName: "taxName", colspan: 2 },
        { displayName: "Effective Date", fieldName: "effectiveDate", colspan: 3 },
        { displayName: "Expiry Date", fieldName: "expiryDate", colspan: 3 },
    ]

    const onEditHandler = (row, event) => {
        console.log(row, event)
    }
    const onDeleteHandler = (row, event) => {
        console.log(row, event)
        setConfirmation(true)
    }
    const onButtonHandler = (row, event) => {
        console.log(row, event);
    }
    const actionList = [
        { type: "icon", iconName: "edit", handler: onEditHandler },
        { type: "icon", iconName: "delete", handler: onDeleteHandler },
    ]

    return (
        <>
            <CoreGrid
                loading={props.loading}
                columns={columns}
                data={props.taxes}
                needActions={true}
                actionList={actionList}
                actionColspan={2}
            />
        </>
    )
}

export default connect(
    (state, props) => ({
        taxes: state.tax.taxes,
        loading: state.tax.loading
    }),
    (dispatch, props) => ({
        loadTaxes: dispatch(getAllTaxes())
    })
)(TaxTable)
