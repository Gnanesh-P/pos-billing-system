import React from 'react'
import CoreDialog from 'app/components/CoreDialog/CoreDialog'
import { Button } from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { addNewTax } from '../store/TaxActions';

function AddTax(props) {
    const [open, setOpen] = React.useState(false);

    const onSave = () => {
        setOpen(false);
        props.addNewTax(formValue)
    }
    const onCancel = () => {
        setOpen(false);
    }

    React.useEffect(() => {
        setFormValue({})
        setErrorValue({})
    }, [open])

    const [formValue, setFormValue] = React.useState({
        taxCode: null,
        taxName: null,
        taxId: null,
        percentage: null,
        effectiveDate: null,
        expiryDate: null
    })

    const [errorValue, setErrorValue] = React.useState({})

    const handleOnChange = (fieldName, event) => {
        formValue[fieldName] = event.target.value;
        setFormValue({ ...formValue })
    }

    return (
        <>
            <Button variant="contained" onClick={() => setOpen(true)} color="primary">ADD TAX</Button>

            <CoreDialog
                open={open}
                handleClose={onCancel}
                handleCancel={onCancel}
                handleSave={onSave}
                modelTitle="Create Tax"
            >
                <Grid container spacing={3}  >
                    <Grid item xs={12} sm={6}>
                        Tax Code
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.taxCode} onChange={e => handleOnChange("taxCode", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Name
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.taxName} onChange={e => handleOnChange("taxName", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Percentage
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField type="number" value={formValue.percentage} onChange={e => handleOnChange("percentage", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Effective Date
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField type="date" value={formValue.effectiveDate} onChange={e => handleOnChange("effectiveDate", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Expiry Date
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField type="date" value={formValue.expiryDate} onChange={e => handleOnChange("expiryDate", e)} variant="outlined" />
                    </Grid>
                </Grid>
            </CoreDialog>
        </>
    )
}

export default connect(
    (state, props) => ({}),
    (dispatch, props) => ({
        addNewTax: (x) => dispatch(addNewTax(x))
    })
)(AddTax)