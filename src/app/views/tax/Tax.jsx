import React from 'react'
import { Card } from '@material-ui/core'
import TaxTable from './shared/TaxTable';
import AddTax from './shared/AddUnit';

export default function Tax() {
    return (
        <Card elevation={3} className="pt-5 mb-6" style={{ padding: "10px", margin: "10px" }}>
            <div className="flex justify-between items-center px-6 mb-3">
                <span className="card-title">Tax</span>
                <div className="flex items-center" >
                    <span className="flex items-center" >
                        <AddTax></AddTax>
                    </span>
                </div>
            </div>

            <TaxTable />
        </Card>
    )
}
