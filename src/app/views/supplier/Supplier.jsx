
import React from 'react'
import { Card } from '@material-ui/core'
import AddSupplier from './shared/AddSupplier'
import SupplierTable from './shared/SupplierTable'

export default function Supplier() {
    return (
        <Card elevation={3} className="pt-5 mb-6" style={{ padding: "10px", margin: "10px" }}>

            <div className="flex justify-between items-center px-6 mb-3">
                <span className="card-title">Supplier</span>
                <div className="flex items-center" >
                    <span className="flex items-center" >
                        <AddSupplier></AddSupplier>
                    </span>
                </div>
            </div>

            <SupplierTable />
        </Card>
    )
}
