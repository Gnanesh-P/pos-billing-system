export const SupplierColumns = [
    { displayName: "Supplier Code", fieldName: "supplierCode", colspan: 2 },
    { displayName: "Name", fieldName: "supplierName", colspan: 3 },
    { displayName: "Contact", fieldName: "supplierContact", colspan: 3 },
]