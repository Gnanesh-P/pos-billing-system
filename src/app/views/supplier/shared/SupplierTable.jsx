import React, { } from 'react'
import { connect } from 'react-redux';
import CoreGrid from 'app/components/CoreGrid/CoreGrid';
import { getAllSupplier } from '../store/SupplierActions';
import { SupplierColumns } from './SupplierColumns';

function SupplierTable({ suppliers, loading }) {

    const onEditHandler = (row, event) => {
        console.log(row, event)
    }
    const onDeleteHandler = (row, event) => {
        console.log(row, event)
    }
    const onButtonHandler = (row, event) => {
        console.log(row, event);
    }
    const actionList = [
        { type: "icon", iconName: "edit", handler: onEditHandler },
        { type: "icon", iconName: "delete", handler: onDeleteHandler },
    ]

    return (
        <>
            <CoreGrid
                loading={loading}
                columns={SupplierColumns}
                data={suppliers}
                needActions={true}
                actionList={actionList}
                actionColspan={2}
            />
        </>
    )
}

export default connect(
    (state, props) => ({
        suppliers: state.supplier.suppliers,
        loading: state.supplier.loading
    }),
    (dispatch, props) => ({
        loadSupplier: dispatch(getAllSupplier())
    })
)(SupplierTable)
