import React from 'react'
import CoreDialog from 'app/components/CoreDialog/CoreDialog'
import { Button } from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { addNewSupplier } from '../store/SupplierActions';

function AddSupplier({ addNewSupplier }) {
    const [open, setOpen] = React.useState(false);

    const onSave = () => {
        setOpen(false);
        addNewSupplier(formValue)
    }
    const onCancel = () => {
        setOpen(false);
    }
    const tempFormValue = {
        supplierId: null,
        supplierCode: null,
        supplierContact: null,
        supplierName: null
    }
    React.useEffect(() => {
        setFormValue({ ...tempFormValue })
        setErrorValue({})
    }, [open])

    const [formValue, setFormValue] = React.useState({ ...tempFormValue })
    const [errorValue, setErrorValue] = React.useState({})

    const handleOnChange = (fieldName, event) => {
        formValue[fieldName] = event.target.value;
        setFormValue({ ...formValue })
    }

    return (
        <>
            <Button variant="contained" onClick={() => setOpen(true)} color="primary">ADD SUPPLIER</Button>

            <CoreDialog
                open={open}
                handleClose={onCancel}
                handleCancel={onCancel}
                handleSave={onSave}
                modelTitle="Create Supplier"
            >
                <Grid container spacing={3}  >
                    <Grid item xs={12} sm={6}>
                        Supplier Code
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.supplierCode} onChange={e => handleOnChange("supplierCode", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Name
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.supplierName} onChange={e => handleOnChange("supplierName", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Contact
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField type="number" value={formValue.supplierContact} onChange={e => handleOnChange("supplierContact", e)} variant="outlined" />
                    </Grid>

                </Grid>
            </CoreDialog>
        </>
    )
}

export default connect(
    (state, props) => ({}),
    (dispatch, props) => ({
        addNewSupplier: (supplier) => dispatch(addNewSupplier(supplier))
    })
)(AddSupplier)