import axios from 'axios'
import { coreHttpGet, coreHttpPost } from 'axios-utils';

export const supplierActions = {
    getAllSupplier: "supplier/get-all-supplier",
}

export const getAllSupplier = () => dispatch => {
    dispatch({
        type: supplierActions.getAllSupplier,
        payload: {
            loading: true,
            items: []
        }
    })

    coreHttpGet({ url: "/suppliers" }).then(data => {
        dispatch({
            type: supplierActions.getAllSupplier,
            payload: {
                loading: false,
                items: data.data
            }
        })
    }).catch(err => {
        dispatch({
            type: supplierActions.getAllSupplier,
            payload: {
                loading: false,
                items: []
            }
        })
    })
}

export const addNewSupplier = (supplier) => dispatch => {
    coreHttpPost({ url: "/suppliers", data: supplier }).then(response => {
        dispatch(getAllSupplier())
    }).catch(err => { console.log(err) })
}