import { supplierActions } from "./SupplierActions"

const initialState = {
    suppliers: [],
    loading: false
}

const SupplierReducer = function(state = initialState, action) {
    switch (action.type) {
        case supplierActions.getAllSupplier:
            return {
                ...state,
                suppliers: [...action.payload.items],
                loading: action.payload.loading
            }
        default:
            return {...state }
    }
}

export default SupplierReducer