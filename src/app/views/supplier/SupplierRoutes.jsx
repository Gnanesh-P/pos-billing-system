import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const SupplierRoutes = [
    {
        path: '/supplier',
        component: React.lazy(() => import('./Supplier')),
        auth: authRoles.sa,
    }
]

export default SupplierRoutes
