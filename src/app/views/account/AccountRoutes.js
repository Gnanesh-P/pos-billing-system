import React from 'react'
import { authRoles } from '../../auth/authRoles'

const accountRoutings = [
    {
        path: '/account',
        component: React.lazy(() => import('./Account')),
        auth: authRoles.sa,
    }
]

export default accountRoutings
