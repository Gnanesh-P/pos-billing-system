import React, { Fragment } from 'react'
import { Grid, Card, Icon, IconButton, Tooltip } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import AccountTable from './shared/AccountTable'

const useStyles = makeStyles(({ palette, ...theme }) => ({
    icon: {
        fontSize: '44px',
        opacity: 0.6,
        color: palette.primary.main,
    },
}))


const Account = () => {
    const classes = useStyles()

    return (
        <Grid container spacing={3} className="p-3">
            <Grid item xs={12}>

            </Grid>
            <Grid item xs={12} md={6}>
                <Card
                    className="flex flex-wrap justify-between align-center p-sm-24 bg-paper"
                    elevation={6}
                >
                    <div className="flex items-center">
                        <Icon className={classes.icon}>attach_money</Icon>
                        <div className="ml-3">
                            <small className="text-muted line-height-1">
                                Today Sales
                            </small>
                            <h6 className="m-0 mt-1 text-primary font-medium">
                                $500
                            </h6>
                        </div>
                    </div>
                    <Tooltip title="View Details" placement="top">
                        <IconButton>
                            {/* <Icon>arrow_right_alt</Icon> */}
                        </IconButton>
                    </Tooltip>
                </Card>
            </Grid>
            <Grid item xs={12} md={6}>
                <Card
                    className="flex flex-wrap justify-between align-center p-sm-24 bg-paper"
                    elevation={6}
                >
                    <div className="flex items-center">
                        <Icon className={classes.icon}>attach_money</Icon>
                        <div className="ml-3">
                            <small className="text-muted line-height-1">
                                This week Sales
                            </small>
                            <h6 className="m-0 mt-1 text-primary font-medium">
                                $80,500
                            </h6>
                        </div>
                    </div>
                    <Tooltip title="View Details" placement="top">
                        <IconButton>
                            {/* <Icon>arrow_right_alt</Icon> */}
                        </IconButton>
                    </Tooltip>
                </Card>
            </Grid>

            <Grid item xs={12}>
                <AccountTable></AccountTable>
            </Grid>

        </Grid>
    )
}

export default Account
