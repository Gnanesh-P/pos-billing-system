import React from 'react'
import { Card } from '@material-ui/core'
import EmployeeTable from './shared/EmployeeTable';
import AddEmployee from './shared/AddEmployee';

export default function Employee() {
    return (
        <Card elevation={3} className="pt-5 mb-6" style={{ padding: "10px", margin: "10px" }}>
            <div className="flex justify-between items-center px-6 mb-3">
                <span className="card-title">Employee</span>
                <div className="flex items-center" >
                    <span className="flex items-center" >
                        <AddEmployee></AddEmployee>
                    </span>
                </div>
            </div>

            <EmployeeTable />
        </Card>
    )
}
