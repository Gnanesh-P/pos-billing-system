import React, { } from 'react'
import { connect } from 'react-redux';
import CoreGrid from 'app/components/CoreGrid/CoreGrid';
import { getAllEmployees } from '../store/EmployeeActions';

function EmployeeTable(props) {

    const [confirmation, setConfirmation] = React.useState(false)

    const columns = [
        { displayName: "Employee Code", fieldName: "employeeCode", colspan: 3 },
        { displayName: "Name", fieldName: "employeeName", colspan: 3 },
        { displayName: "Contact Number", fieldName: "contactNumber", colspan: 3 },
    ]

    const onEditHandler = (row, event) => {
        console.log(row, event)
    }
    const onDeleteHandler = (row, event) => {
        console.log(row, event)
        setConfirmation(true)
    }
    const onButtonHandler = (row, event) => {
        console.log(row, event);
    }
    const actionList = [
        { type: "icon", iconName: "edit", handler: onEditHandler },
        { type: "icon", iconName: "delete", handler: onDeleteHandler },
    ]

    return (
        <>
            <CoreGrid
                loading={props.loading}
                columns={columns}
                data={props.employees}
                needActions={true}
                actionList={actionList}
                actionColspan={2}
            />
        </>
    )
}

export default connect(
    (state, props) => ({
        employees: state.employee.employees,
        loading: state.employee.loading
    }),
    (dispatch, props) => ({
        loadEmployees: dispatch(getAllEmployees())
    })
)(EmployeeTable)
