import React from 'react'
import CoreDialog from 'app/components/CoreDialog/CoreDialog'
import { Button } from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { addNewEmployee } from '../store/EmployeeActions';

function AddEmployee(props) {
    const [open, setOpen] = React.useState(false);

    const onSave = () => {
        setOpen(false);
        props.addNewEmployee(formValue)
    }
    const onCancel = () => {
        setOpen(false);
    }

    React.useEffect(() => {
        setFormValue({ ...tempFormValue })
        setErrorValue({})
    }, [open])

    const tempFormValue = {
        employeeCode: null,
        employeeName: null,
        employeeId: null,
        contactNumber: null
    }

    const [formValue, setFormValue] = React.useState({ ...tempFormValue })
    const [errorValue, setErrorValue] = React.useState({})

    const handleOnChange = (fieldName, event) => {
        formValue[fieldName] = event.target.value;
        setFormValue({ ...formValue })
    }

    return (
        <>
            <Button variant="contained" onClick={() => setOpen(true)} color="primary">ADD EMPLOYEE</Button>

            <CoreDialog
                open={open}
                handleClose={onCancel}
                handleCancel={onCancel}
                handleSave={onSave}
                modelTitle="Create Employee"
            >
                <Grid container spacing={3}  >
                    <Grid item xs={12} sm={6}>
                        Employee Code
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.employeeCode} onChange={e => handleOnChange("employeeCode", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Name
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.employeeName} onChange={e => handleOnChange("employeeName", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Contact Number
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.contactNumber} onChange={e => handleOnChange("contactNumber", e)} variant="outlined" />
                    </Grid>
                </Grid>
            </CoreDialog>
        </>
    )
}

export default connect(
    (state, props) => ({}),
    (dispatch, props) => ({
        addNewEmployee: (x) => dispatch(addNewEmployee(x))
    })
)(AddEmployee)