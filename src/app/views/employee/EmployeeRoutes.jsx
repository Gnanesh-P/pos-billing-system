import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const EmployeeRoutes = [
    {
        path: '/employee',
        component: React.lazy(() => import('./Employee')),
        auth: authRoles.sa,
    }
]

export default EmployeeRoutes
