import { utilCopy } from 'utils'

export const employeeActions = {
    getAllEmployees: "employee/get-all-employees",
    addNewEmployee: "employee/add-new-employee"
}

export const getAllEmployees = () => dispatch => {
    dispatch({
        type: employeeActions.getAllEmployees,
        payload: {
            items: [],
            loading: true
        }
    })
    setTimeout(() => {
        dispatch({
            type: employeeActions.getAllEmployees,
            payload: {
                items: utilCopy([
                    {
                        employeeCode: "Dewe",
                        employeeName: "Dow",
                        employeeId: "wer",
                        contactNumber: 234234
                    },
                    {
                        employeeCode: "234",
                        employeeName: "Do234w",
                        employeeId: "wer34",
                        contactNumber: 234234
                    }
                ]),
                loading: false
            }
        })
    }, 1000);
}


export const addNewEmployee = (employee) => dispatch => {
    dispatch({
        type: employeeActions.addNewEmployee,
        payload: employee
    })
}