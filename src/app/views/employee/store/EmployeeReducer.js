import { employeeActions } from "./EmployeeActions"

const initialState = {
    employees: [],
    loading: false
}

const EmployeeReducer = function (state = initialState, action) {
    switch (action.type) {
        case employeeActions.getAllEmployees:
            return {
                ...state,
                employees: [...action.payload.items],
                loading: action.payload.loading
            }

        case employeeActions.addNewEmployee:
            return {
                ...state,
                employees: [...state.employees, action.payload],
            }
        default:
            return { ...state }
    }
}

export default EmployeeReducer