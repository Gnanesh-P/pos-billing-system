import { coreHttpGet, coreHttpPost } from 'axios-utils';

export const transactionActions = {
    getAllTransaction: "transaction/get-all-transaction",
}

export const getAllTransaction = () => dispatch => {
    dispatch({
        type: transactionActions.getAllTransaction,
        payload: {
            loading: true,
            items: []
        }
    })

    coreHttpGet({ url: "/stocks" }).then(data => {
        dispatch({
            type: transactionActions.getAllTransaction,
            payload: {
                loading: false,
                items: data.data
            }
        })
    }).catch(err => {
        dispatch({
            type: transactionActions.getAllTransaction,
            payload: {
                loading: false,
                items: []
            }
        })
    })
}

export const addNewTransaction = (transaction) => dispatch => {
    coreHttpPost({ url: "/stocks", data: transaction }).then(response => {
        dispatch(getAllTransaction())
    }).catch(err => { console.log(err) })
}