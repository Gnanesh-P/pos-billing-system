import { transactionActions } from "./TransactionActions"

const initialState = {
    transactions: [],
    loading: false
}

const TransactionReducer = function(state = initialState, action) {
    switch (action.type) {
        case transactionActions.getAllTransaction:
            return {
                ...state,
                transactions: [...action.payload.items],
                loading: action.payload.loading
            }
        default:
            return {...state }
    }
}

export default TransactionReducer