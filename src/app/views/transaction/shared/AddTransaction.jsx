import React from 'react'
import CoreDialog from 'app/components/CoreDialog/CoreDialog'
import { Button } from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { addNewTransaction } from '../store/TransactionActions';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { getAllLocation } from 'app/views/location/store/LocationActions';
import { getAllProducts } from 'app/views/product/redux/ProductActions';
import { getAllSupplier } from 'app/views/supplier/store/SupplierActions';
import { TransactionModel, TransactionTypeArray } from './TransactionModel';
import CoreZoomSelect from 'app/components/CoreZoomSelect/CoreZoomSelect';
import { CategoryColumns } from 'app/views/category/shared/category-columns';
import { LocationColumns } from 'app/views/location/shared/location-columns';
import { UnitColumns } from 'app/views/units/shared/UnitColums';
import { getAllUnits } from 'app/views/units/store/UnitActions';
import { getAllWarehouse } from 'app/views/warehouse/store/WarehouseActions';
import { WarehouseColumns } from 'app/views/warehouse/shared/WarehouseColumns';
import { SupplierColumns } from 'app/views/supplier/shared/SupplierColumns';
import { TransactionType } from "./TransactionModel"

function AddTransaction({
    locations,
    products,
    suppliers,
    units,
    warehouses,

    addNewTransaction,

    loadWarehouses,
    loadUnits,
    loadLocations,
    loadProducts,
    loadSuppliers,
}) {
    const [open, setOpen] = React.useState(false);

    const onSave = () => {
        setOpen(false);
        formValue.createdDateTime = new Date();
        formValue.createdUser = "Sujith";
        addNewTransaction(formValue)
    }
    const onCancel = () => {
        setOpen(false);
    }

    React.useEffect(() => {
        setFormValue({ ...TransactionModel })
        setErrorValue({})
        loadLocations();
        loadProducts();
        loadSuppliers();
        loadUnits();
        loadWarehouses();
    }, [])

    const [formValue, setFormValue] = React.useState({ ...TransactionModel })
    const [errorValue, setErrorValue] = React.useState({})

    const handleOnChange = (fieldName, event) => {
        switch (fieldName) {
            case "product":
                formValue.product = products.find(x => x.productId === event.productId) || {}
                break;
            case "unit":
                formValue.unit = units.find(x => x.unitId === event.unitId) || {}
                break;
            case "location":
                formValue.location = locations.find(x => x.locationId === event.locationId) || {}
                break;
            case "warehouse":
                formValue.warehouse = warehouses.find(x => x.warehouseId === event.warehouseId) || {}
                break;
            case "supplier":
                formValue.supplier = suppliers.find(x => x.supplierId === event.supplierId) || {}
                break;
            case "fromWarehouse":
                formValue.fromWarehouse = warehouses.find(x => x.warehouseId === event.warehouseId) || {}
                break;
            case "toWarehouse":
                formValue.toWarehouse = warehouses.find(x => x.warehouseId === event.warehouseId) || {}
                break;
            case "fromLocation":
                formValue.fromLocation = locations.find(x => x.locationId === event.locationId) || {}
                break;
            case "toLocation":
                formValue.toLocation = locations.find(x => x.locationId === event.locationId) || {}
                break;
            default:
                formValue[fieldName] = event.target.value;
                break;
        }
        setFormValue({ ...formValue })
    }

    const isPURCHASE_RECEIPT = () => formValue.transactionType === TransactionType.PURCHASE_RECEIPT
    const isSALES_ISSUE = () => formValue.transactionType === TransactionType.SALES_ISSUE
    const isTRANSFER = () => formValue.transactionType === TransactionType.TRANSFER
    const isADJUSTMENT = () => formValue.transactionType === TransactionType.ADJUSTMENT
    const isSCRAP = () => formValue.transactionType === TransactionType.SCRAP
    const isRETURN = () => formValue.transactionType === TransactionType.RETURN

    return (
        <>
            <Button variant="contained" onClick={() => setOpen(true)} color="primary">ADD TRANSACTION</Button>

            <CoreDialog
                open={open}
                handleClose={onCancel}
                handleCancel={onCancel}
                handleSave={onSave}
                size={"md"}
                modelTitle="Create Transaction"
            >
                <Grid container spacing={3} >

                    <Grid item sm={3}>
                        Transaction Type
                    </Grid>
                    <Grid item sm={3}>
                        <Select variant="outlined" value={formValue.transactionType} onChange={e => handleOnChange("transactionType", e)} >
                            {TransactionTypeArray.map(x => <MenuItem value={x}>{x}</MenuItem>)}
                        </Select>
                    </Grid>



                    <Grid item sm={3}>
                        Product
                    </Grid>
                    <Grid item sm={3}>
                        <CoreZoomSelect
                            value={formValue?.product?.productId}
                            valueField={"productId"}
                            display={formValue?.product?.productName}
                            columns={CategoryColumns}
                            data={products}
                            size={"md"}
                            title="Choose Product"
                            onSelect={(selectedProduct) => handleOnChange("product", selectedProduct)} >
                        </CoreZoomSelect>
                    </Grid>

                    <Grid item sm={3}>
                        Quantity
                    </Grid>
                    <Grid item sm={3}>
                        <TextField type="number" value={formValue.quantity} onChange={e => handleOnChange("quantity", e)} variant="outlined" />
                    </Grid>

                    <Grid item sm={3}>
                        Unit
                    </Grid>
                    <Grid item sm={3}>
                        <CoreZoomSelect
                            value={formValue?.unit?.unitId}
                            valueField={"unitId"}
                            display={formValue?.unit?.unitName}
                            columns={UnitColumns}
                            data={units}
                            title="Choose Units"
                            onSelect={(item) => handleOnChange("unit", item)} >
                        </CoreZoomSelect>
                    </Grid>

                    {(isPURCHASE_RECEIPT() || isSALES_ISSUE() || isRETURN()) && <>
                        <Grid item sm={3}>
                            Lot Id
                        </Grid>
                        <Grid item sm={3}>
                            <TextField value={formValue.lotId} onChange={e => handleOnChange("lotId", e)} variant="outlined" />
                        </Grid>
                    </>}

                    {(isPURCHASE_RECEIPT() || isADJUSTMENT() || isSCRAP() || isRETURN()) && <>
                        <Grid item sm={3}>
                            Location
                        </Grid>
                        <Grid item sm={3}>
                            <CoreZoomSelect
                                value={formValue?.location?.locationId}
                                valueField={"locationId"}
                                display={formValue?.location?.locationDescription}
                                columns={LocationColumns}
                                data={locations}
                                size={"md"}
                                title="Choose Location"
                                onSelect={(item) => handleOnChange("location", item)} >
                            </CoreZoomSelect>
                        </Grid>
                    </>}

                    {(isPURCHASE_RECEIPT()) && <>
                        <Grid item sm={3}>
                            Expiry Date
                        </Grid>
                        <Grid item sm={3}>
                            <TextField type="date" value={formValue.expiryDate} onChange={e => handleOnChange("expiryDate", e)} variant="outlined" />
                        </Grid>
                    </>}

                    {(isPURCHASE_RECEIPT()) && <>
                        <Grid item sm={3}>
                            Manufacture Date
                        </Grid>
                        <Grid item sm={3}>
                            <TextField type="date" value={formValue.manufactureDate} onChange={e => handleOnChange("manufactureDate", e)} variant="outlined" />
                        </Grid>
                    </>}

                    {(isPURCHASE_RECEIPT() || isADJUSTMENT() || isSCRAP() || isRETURN()) && <>
                        <Grid item sm={3}>
                            Warehouse
                        </Grid>
                        <Grid item sm={3}>
                            <CoreZoomSelect
                                value={formValue?.warehouse?.warehouseId}
                                valueField={"warehouseId"}
                                display={formValue?.warehouse?.warehouseCode}
                                columns={WarehouseColumns({ onWarehouseClick: () => { } })}
                                data={warehouses}
                                size={"md"}
                                title="Choose Warehouse"
                                onSelect={(selectedWH) => handleOnChange("warehouse", selectedWH)} >
                            </CoreZoomSelect>
                        </Grid>
                    </>}


                    {(isPURCHASE_RECEIPT() || isRETURN()) && <>
                        <Grid item sm={3}>
                            Supplier
                        </Grid>
                        <Grid item sm={3}>
                            <CoreZoomSelect
                                value={formValue?.supplier?.supplierId}
                                valueField={"supplierId"}
                                display={formValue?.supplier?.supplierId}
                                columns={SupplierColumns}
                                data={suppliers}
                                size={"md"}
                                title="Choose Supplier"
                                onSelect={(item) => handleOnChange("supplier", item)} >
                            </CoreZoomSelect>
                        </Grid>
                    </>}

                    {(isPURCHASE_RECEIPT() || isRETURN()) && <>
                        <Grid item sm={3}>
                            Order Reference
                        </Grid>
                        <Grid item sm={3}>
                            <TextField value={formValue.orderReference} onChange={e => handleOnChange("orderReference", e)} variant="outlined" />
                        </Grid>
                    </>}

                    {(isPURCHASE_RECEIPT() || isRETURN()) && <>
                        <Grid item sm={3}>
                            Invoice Reference
                        </Grid>
                        <Grid item sm={3}>
                            <TextField value={formValue.invoiceReference} onChange={e => handleOnChange("invoiceReference", e)} variant="outlined" />
                        </Grid>
                    </>}

                    {(isPURCHASE_RECEIPT() || isRETURN()) && <>
                        <Grid item sm={3}>
                            Purchase Price
                        </Grid>
                        <Grid item sm={3}>
                            <TextField type="number" value={formValue.purchasePrice} onChange={e => handleOnChange("purchasePrice", e)} variant="outlined" />
                        </Grid>
                    </>}


                    {(isSALES_ISSUE()) && <>
                        <Grid item sm={3}>
                            Sales Price
                        </Grid>
                        <Grid item sm={3}>
                            <TextField type="number" value={formValue.salesPrice} onChange={e => handleOnChange("salesPrice", e)} variant="outlined" />
                        </Grid>
                    </>}

                    {(isSALES_ISSUE()) && <>
                        <Grid item sm={3}>
                            Bill Number
                        </Grid>
                        <Grid item sm={3}>
                            <TextField value={formValue.billNumber} onChange={e => handleOnChange("billNumber", e)} variant="outlined" />
                        </Grid>
                    </>}

                    {(isTRANSFER()) && <>
                        <Grid item sm={3}>
                            From Warehouse
                        </Grid>
                        <Grid item sm={3}>
                            <CoreZoomSelect
                                value={formValue?.fromWarehouse?.warehouseId}
                                valueField={"warehouseId"}
                                display={formValue?.fromWarehouse?.warehouseCode}
                                columns={WarehouseColumns({ onWarehouseClick: () => { } })}
                                data={warehouses}
                                size={"md"}
                                title="Choose From Warehouse"
                                onSelect={(item) => handleOnChange("fromWarehouse", item)} >
                            </CoreZoomSelect>
                        </Grid>
                    </>}

                    {(isTRANSFER()) && <>
                        <Grid item sm={3}>
                            To Warehouse
                        </Grid>
                        <Grid item sm={3}>
                            <CoreZoomSelect
                                value={formValue?.toWarehouse?.warehouseId}
                                valueField={"warehouseId"}
                                display={formValue?.toWarehouse?.warehouseCode}
                                columns={WarehouseColumns({ onWarehouseClick: () => { } })}
                                data={warehouses}
                                size={"md"}
                                title="Choose To Warehouse"
                                onSelect={(item) => handleOnChange("toWarehouse", item)} >
                            </CoreZoomSelect>
                        </Grid>
                    </>}

                    {(isTRANSFER()) && <>
                        <Grid item sm={3}>
                            From Location
                        </Grid>
                        <Grid item sm={3}>
                            <CoreZoomSelect
                                value={formValue?.fromLocation?.locationId}
                                valueField={"locationId"}
                                display={formValue?.fromLocation?.locationDescription}
                                columns={LocationColumns}
                                data={locations}
                                size={"md"}
                                title="Choose From Location"
                                onSelect={(item) => handleOnChange("fromLocation", item)} >
                            </CoreZoomSelect>
                        </Grid>
                    </>}


                    {(isTRANSFER()) && <>
                        <Grid item sm={3}>
                            To Location
                        </Grid>
                        <Grid item sm={3}>
                            <CoreZoomSelect
                                value={formValue?.toLocation?.locationId}
                                valueField={"locationId"}
                                display={formValue?.toLocation?.locationDescription}
                                columns={LocationColumns}
                                data={locations}
                                size={"md"}
                                title="Choose To Location"
                                onSelect={(item) => handleOnChange("toLocation", item)} >
                            </CoreZoomSelect>
                        </Grid>
                    </>}


                    {(isADJUSTMENT() || isSCRAP()) && <>
                        < Grid item sm={3}>
                            Current Quantity
                        </Grid>
                        <Grid item sm={3}>
                            <TextField type="number" value={formValue.currentQuantity} onChange={e => handleOnChange("currentQuantity", e)} variant="outlined" />
                        </Grid>
                    </>}


                    {(isADJUSTMENT() || isSCRAP()) && <>
                        <Grid item sm={3}>
                            New Quantity
                        </Grid>
                        <Grid item sm={3}>
                            <TextField type="number" value={formValue.newQuantity} onChange={e => handleOnChange("newQuantity", e)} variant="outlined" />
                        </Grid>
                    </>}
                </Grid>
            </CoreDialog>
        </>
    )
}

export default connect(
    (state, props) => ({
        suppliers: state.supplier.suppliers,
        locations: state.location.locations,
        products: state.product.products,
        units: state.unit.units,
        warehouses: state.warehouse.warehouses
    }),
    (dispatch, props) => ({
        addNewTransaction: (supplier) => dispatch(addNewTransaction(supplier)),
        loadLocations: () => dispatch(getAllLocation()),
        loadProducts: () => dispatch(getAllProducts()),
        loadSuppliers: () => dispatch(getAllSupplier()),
        loadUnits: () => dispatch(getAllUnits()),
        loadWarehouses: () => dispatch(getAllWarehouse())
    })
)(AddTransaction)