export const TransactionType = {
    PURCHASE_RECEIPT: "PURCHASE_RECEIPT",
    SALES_ISSUE: "SALES_ISSUE",
    TRANSFER: "TRANSFER",
    ADJUSTMENT: "ADJUSTMENT",
    SCRAP: "SCRAP",
    RETURN: "RETURN"
}

export const TransactionTypeArray = [
    TransactionType.PURCHASE_RECEIPT,
    TransactionType.SALES_ISSUE,
    TransactionType.TRANSFER,
    TransactionType.ADJUSTMENT,
    TransactionType.SCRAP,
    TransactionType.RETURN,
]

export const TransactionModel = {
    transactionType: null,
    transactionId: null,
    lotId: null,
    product: {
        productId: null,
        productCode: null,
        productBuyingPrice: null,
        productName: null,
        productSellingPrice: null,
        category: {
            categoryId: null,
            categoryName: null,
            categoryCode: null,
        },
        traceable: null
    },
    quantity: null,
    unit: {
        unitCode: null,
        unitName: null,
        unitId: null,
    },
    expiryDate: null,
    manufactureDate: null,
    location: {
        locationId: null,
        locationCode: null,
        locationDescription: null,
        availableSpace: null,
        occupiedSpace: null,
        warehouse: {
            id: null,
            warehouseId: null,
            warehouseLocation: null,
            warehouseAddress: null,
            managerName: null,
            managerContactNumber: null,
        },
    },
    warehouse: {
        warehouseId: null,
        warehouseAddress: null,
        warehouseCode: null,
        city: null,
        state: null,
        country: null,
        employee: {
            employeeCode: null,
            employeeName: null,
            employeeId: null,
            contactNumber: null
        }
    },
    supplier: {
        supplierId: null,
        supplierCompany: null,
        supplierContact: null,
        supplierName: null,
    },
    orderReference: null,
    invoiceReference: null,
    purchasePrice: null,
    salesPrice: null,
    billNumber: null,
    fromWarehouse: {
        warehouseId: null,
        warehouseAddress: null,
        warehouseCode: null,
        city: null,
        state: null,
        country: null,
        employee: {
            employeeCode: null,
            employeeName: null,
            employeeId: null,
            contactNumber: null
        }
    },
    toWarehouse: {
        warehouseId: null,
        warehouseAddress: null,
        warehouseCode: null,
        city: null,
        state: null,
        country: null,
        employee: {
            employeeCode: null,
            employeeName: null,
            employeeId: null,
            contactNumber: null
        }
    },
    fromLocation: {
        locationId: null,
        locationCode: null,
        locationDescription: null,
        availableSpace: null,
        occupiedSpace: null,
        warehouse: {
            id: null,
            warehouseId: null,
            warehouseLocation: null,
            warehouseAddress: null,
            managerName: null,
            managerContactNumber: null,
        },
    },
    toLocation: {
        locationId: null,
        locationCode: null,
        locationDescription: null,
        availableSpace: null,
        occupiedSpace: null,
        warehouse: {
            id: null,
            warehouseId: null,
            warehouseLocation: null,
            warehouseAddress: null,
            managerName: null,
            managerContactNumber: null,
        },
    },
    currentQuantity: null,
    newQuantity: null,
    processDateTime: null,
    createdDateTime: null,
    createdUser: null,
}