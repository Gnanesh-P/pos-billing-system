import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import CoreGrid from 'app/components/CoreGrid/CoreGrid';
import { getAllTransaction } from '../store/TransactionActions';
import { TransactionColumns } from './TransactionColumns';

function TransactionTable({ transactions, loading, loadTransaction }) {

    useEffect(() => {
        loadTransaction()
    }, [])

    const onEditHandler = (row, event) => {
        console.log(row, event)
    }
    const onDeleteHandler = (row, event) => {
        console.log(row, event)
    }

    const actionList = [
        { type: "icon", iconName: "edit", handler: onEditHandler },
        { type: "icon", iconName: "delete", handler: onDeleteHandler },
    ]

    return (
        <>
            <CoreGrid
                loading={loading}
                columns={TransactionColumns}
                data={transactions}
                needActions={true}
                actionList={actionList}
                actionColspan={2}
                extendColumn={true}
            />
        </>
    )
}

export default connect(
    (state, props) => ({
        transactions: state.transaction.transactions,
        loading: state.transaction.loading
    }),
    (dispatch, props) => ({
        loadTransaction: () => dispatch(getAllTransaction())
    })
)(TransactionTable)
