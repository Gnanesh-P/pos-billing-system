
import React from 'react'
import { Card } from '@material-ui/core'
import AddTransaction from './shared/AddTransaction'
import TransactionTable from './shared/TransactionTable'

export default function Transaction() {
    return (
        <Card elevation={3} className="pt-5 mb-6" style={{ padding: "10px", margin: "10px" }}>

            <div className="flex justify-between items-center px-6 mb-3">
                <span className="card-title">Transaction</span>
                <div className="flex items-center" >
                    <span className="flex items-center" >
                        <AddTransaction></AddTransaction>
                    </span>
                </div>
            </div>

            <TransactionTable />
        </Card>
    )
}
