import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const TransactionRoutes = [
    {
        path: '/transactions',
        component: React.lazy(() => import('./Transaction')),
        auth: authRoles.sa,
    }
]

export default TransactionRoutes
