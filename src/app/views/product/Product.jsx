
import React from 'react'
import { Button, Card } from '@material-ui/core'
import ProductTable from './shared/ProductTable'
import AddProduct from './shared/AddProduct'

export default function Product() {
    return (
        <Card elevation={3} className="pt-5 mb-6" style={{ padding: "10px", margin: "10px" }}>

            <div className="flex justify-between items-center px-6 mb-3">
                <span className="card-title">Product</span>
                <div className="flex items-center" >
                    <span className="flex items-center" >
                        <AddProduct></AddProduct>
                    </span>
                </div>
            </div>

            <ProductTable />
        </Card>
    )
}
