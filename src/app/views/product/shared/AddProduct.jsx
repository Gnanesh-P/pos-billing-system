import React from 'react'
import CoreDialog from 'app/components/CoreDialog/CoreDialog'
import { Button } from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { addNewProduct } from '../redux/ProductActions';
import Checkbox from '@material-ui/core/Checkbox';
import { getAllCategories } from 'app/views/category/store/CategoryActions';
import CoreZoomSelect from 'app/components/CoreZoomSelect/CoreZoomSelect';
import { getAllUnits } from 'app/views/units/store/UnitActions'
import { getAllTaxes } from 'app/views/tax/store/TaxActions';

function AddProduct({
    categories,
    units,
    taxes,

    loadTaxes,
    addNewProduct,
    loadCategories,
    loadUnits,
}) {
    const [open, setOpen] = React.useState(false);
    const categoryColumns = [
        { displayName: "Category Code", fieldName: "categoryCode", colspan: 3 },
        { displayName: "Name", fieldName: "categoryName", colspan: 3 },
    ]
    const unitColumns = [
        { displayName: "Unit Code", fieldName: "unitCode", colspan: 3 },
        { displayName: "Name", fieldName: "unitName", colspan: 3 },
    ]

    const taxColumns = [
        { displayName: "Tax Code", fieldName: "taxCode", colspan: 2 },
        { displayName: "Percentage", fieldName: "percentage", colspan: 2 },
        { displayName: "Name", fieldName: "taxName", colspan: 2 },
        { displayName: "Effective Date", fieldName: "effectiveDate", colspan: 3 },
        { displayName: "Expiry Date", fieldName: "expiryDate", colspan: 3 },
    ]

    const onSave = () => {
        setOpen(false);
        addNewProduct(formValue)
    }
    const onCancel = () => {
        setOpen(false);
    }

    const tempFormValue = {
        productCode: null,
        productName: null,
        productBuyingPrice: null,
        productSellingPrice: null,
        traceable: false,
        refillQuantity: null,
        unit: {
            unitId: null,
            unitCode: null,
            unitName: null
        },
        category: {
            categoryId: null,
            categoryName: null,
            categoryCode: null,
            tax: {
                taxCode: null,
                taxName: null,
                taxId: null,
                effectiveDate: null,
                expiryDate: null,
                percentage: null
            }
        },
        tax: {
            taxCode: null,
            taxName: null,
            taxId: null,
            effectiveDate: null,
            expiryDate: null,
            percentage: null
        }
    }

    React.useEffect(() => {
        setFormValue({ ...tempFormValue })
        setErrorValue({})
        loadCategories();
        loadUnits();
        loadTaxes();
    }, [open])

    const [formValue, setFormValue] = React.useState({ ...tempFormValue })

    const [errorValue, setErrorValue] = React.useState({})

    const handleOnChange = (fieldName, event) => {
        if (fieldName === 'traceable') {
            formValue[fieldName] = !!!formValue[fieldName]
        }
        else if (fieldName === 'category') {
            formValue[fieldName] = categories.find(x => x.categoryId === event.categoryId) || {}
        }
        else if (fieldName === 'unit') {
            formValue[fieldName] = units.find(x => x.unitId === event.unitId) || {}
        } else if (fieldName === "tax") {
            formValue[fieldName] = taxes.find(x => x.taxId == event.taxId)
        } else {
            formValue[fieldName] = event.target.value;
        }
        console.log(formValue)
        setFormValue({ ...formValue })
    }

    return (
        <>
            <Button variant="contained" onClick={() => setOpen(true)} color="primary">ADD PRODUCT</Button>

            <CoreDialog
                open={open}
                handleClose={onCancel}
                handleCancel={onCancel}
                handleSave={onSave}
                size={"md"}
                modelTitle="Create Product"
            >
                <Grid container spacing={3}  >
                    <Grid item xs={12} sm={6}>
                        Product Code
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField type="number" value={formValue.categoryCode} onChange={e => handleOnChange("productCode", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Name
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.categoryName} onChange={e => handleOnChange("productName", e)} variant="outlined" />
                    </Grid>


                    <Grid item xs={12} sm={6}>
                        Buying Price
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField type="number" value={formValue.productBuyingPrice} onChange={e => handleOnChange("productBuyingPrice", e)} variant="outlined" />
                    </Grid>


                    <Grid item xs={12} sm={6}>
                        Selling Price
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField type="number" value={formValue.productSellingPrice} onChange={e => handleOnChange("productSellingPrice", e)} variant="outlined" />
                    </Grid>


                    <Grid item xs={12} sm={6}>
                        Traceable
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Checkbox checked={formValue.traceable} color="primary" onChange={e => handleOnChange("traceable", e)} />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Refill Quantity
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField type="number" value={formValue.refillQuantity} onChange={e => handleOnChange("refillQuantity", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Category
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <CoreZoomSelect
                            value={formValue?.category?.categoryId}
                            valueField={"categoryId"}
                            display={formValue?.category?.categoryName}
                            columns={categoryColumns}
                            data={categories}
                            size={"sm"}
                            title="Choose Category"
                            onSelect={(selectedCategory) => handleOnChange("category", selectedCategory)} >
                        </CoreZoomSelect>
                    </Grid>


                    <Grid item xs={12} sm={6}>
                        Unit
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <CoreZoomSelect
                            value={formValue?.unit?.unitId}
                            valueField={"unitId"}
                            display={formValue?.unit?.unitName}
                            columns={unitColumns}
                            data={units}
                            size={"sm"}
                            title="Choose Unit"
                            onSelect={(selectedUnit) => handleOnChange("unit", selectedUnit)} >
                        </CoreZoomSelect>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Tax
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <CoreZoomSelect
                            value={formValue?.tax?.taxId}
                            valueField={"taxId"}
                            display={formValue?.tax?.taxName}
                            columns={taxColumns}
                            data={taxes}
                            size={"md"}
                            title="Choose Tax"
                            onSelect={(selectedTax) => handleOnChange("tax", selectedTax)} >
                        </CoreZoomSelect>
                    </Grid>

                </Grid>
            </CoreDialog>
        </>
    )
}

export default connect(
    (state, props) => ({
        categories: state.category.categories,
        units: state.unit.units,
        taxes: state.tax.taxes
    }),
    (dispatch, props) => ({
        addNewProduct: (product) => dispatch(addNewProduct(product)),
        loadCategories: () => dispatch(getAllCategories()),
        loadUnits: () => dispatch(getAllUnits()),
        loadTaxes: () => dispatch(getAllTaxes())
    })
)(AddProduct)