import React, { } from 'react'
import { connect } from 'react-redux';
import { getAllProducts } from 'app/views/product/redux/ProductActions';
import CoreGrid from 'app/components/CoreGrid/CoreGrid';

function ProductTable({ products, loading }) {

    const columns = [
        { displayName: "Product Code", fieldName: "productCode", colspan: 2 },
        { displayName: "Name", fieldName: "productName", colspan: 3 },
        { displayName: "Refill Quantity", fieldName: "refillQuantity", colspan: 2 },
        { displayName: "Category", fieldName: "categoryName", colspan: 3, value: (row) => row.category && row.category.categoryName ? row.category.categoryName : "" },
        { displayName: "Tax", fieldName: "taxName", colspan: 2, value: (row) => row.tax && row.tax.taxName ? row.tax.taxName : "" },
        { displayName: "Unit", fieldName: "unitName", colspan: 1, value: (row) => row.unit && row.unit.unitName ? row.unit.unitName : "" },
        { displayName: "Buying Price", fieldName: "productBuyingPrice", colspan: 2 },
        { displayName: "Selling Price", fieldName: "productSellingPrice", colspan: 2 },
    ]

    const onEditHandler = (row, event) => {
        console.log(row, event)
    }
    const onDeleteHandler = (row, event) => {
        console.log(row, event)
    }
    const onButtonHandler = (row, event) => {
        console.log(row, event);
    }
    const actionList = [
        { type: "icon", iconName: "edit", handler: onEditHandler },
        { type: "icon", iconName: "delete", handler: onDeleteHandler },
    ]


    return (
        <>
            <CoreGrid
                loading={loading}
                columns={columns}
                data={products}
                needActions={true}
                actionList={actionList}
                actionColspan={2}
            />
        </>
    )
}

export default connect(
    (state, props) => ({
        products: state.product.products,
        loading: state.product.loading
    }),
    (dispatch, props) => ({
        loadProduct: dispatch(getAllProducts())
    })
)(ProductTable)
