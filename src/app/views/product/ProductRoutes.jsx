import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const ProductRoutes = [
    {
        path: '/product',
        component: React.lazy(() => import('./Product')),
        auth: authRoles.sa,
    }
]

export default ProductRoutes
