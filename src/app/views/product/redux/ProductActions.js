import axios from 'axios'
import { coreHttpGet, coreHttpPost } from 'axios-utils';
import { utilCopy } from 'utils';

export const productActions = {
    getAllProducts: "product/get-all-product",
}

export const getAllProducts = () => dispatch => {
    dispatch({
        type: productActions.getAllProducts,
        payload: {
            loading: true,
            items: []
        }
    })

    coreHttpGet({ url: "/categories/products/products" }).then(data => {
        dispatch({
            type: productActions.getAllProducts,
            payload: {
                loading: false,
                items: data.data
            }
        })
    }).catch(err => {
        dispatch({
            type: productActions.getAllProducts,
            payload: {
                loading: false,
                items: []
            }
        })
    })
}

export const addNewProduct = (product) => dispatch => {
    coreHttpPost({ url: "/categories/products/products", data: product }).then(response => {
        dispatch(getAllProducts())
    }).catch(err => { console.log(err) })
}