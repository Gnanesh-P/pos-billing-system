import { productActions } from "./ProductActions"

const initialState = {
    products: [],
    loading: false
}

const ProductReducer = function(state = initialState, action) {
    switch (action.type) {
        case productActions.getAllProducts:
            return {
                ...state,
                products: [...action.payload.items],
                loading: action.payload.loading
            }
        default:
            return {...state }
    }
}

export default ProductReducer