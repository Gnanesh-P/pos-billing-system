import React from 'react'
import { authRoles } from 'app/auth/authRoles'

const CustomerRoutes = [
    {
        path: '/customer',
        component: React.lazy(() => import('./Customer')),
        auth: authRoles.sa,
    }
]

export default CustomerRoutes
