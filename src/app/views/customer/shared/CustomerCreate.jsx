import React, { useEffect } from 'react'
import { Button, Card } from '@material-ui/core'
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { connect } from 'react-redux';
import { createCustomer } from 'app/redux/page/customer/CustomerActions';

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

function CustomerCreate(props) {
    const { handleOpenCustomerCreate,
        handleCloseCustomerCreate,
        customerCreate, createCustomer
    } = props

    useEffect(() => {
        setFormValue({})
        setErrorValue({})
    }, [handleOpenCustomerCreate])
    const [formValue, setFormValue] = React.useState({
        name: "",
        mobile: "",
        gender: "",
        age: "",
        address: ""
    })
    const [errorValue, setErrorValue] = React.useState({})

    const handleOnChange = (fieldName, event) => {
        formValue[fieldName] = event.target.value;
        setFormValue({ ...formValue })
    }

    return (
        <Dialog onClose={handleCloseCustomerCreate} open={customerCreate} >
            <DialogTitle onClose={handleCloseCustomerCreate}>
                Add New Customer
            </DialogTitle>
            <DialogContent dividers>
                <Grid container spacing={3} style={{ width: 560 }}>
                    <Grid item xs={12} sm={6}>
                        Name
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.name} onChange={e => handleOnChange("name", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Mobile Number
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField type="number" value={formValue.mobile} onChange={e => handleOnChange("mobile", e)} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Gender
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Select
                            variant="outlined"
                            value={formValue?.gender}
                            onChange={e => handleOnChange("gender", e)}
                        >
                            <MenuItem selected value={"MALE"}>Male</MenuItem>
                            <MenuItem value={"FEMALE"}>FeMale</MenuItem>
                            <MenuItem value={"OTHERS"}>Others</MenuItem>
                        </Select>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Age
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.age} onChange={e => handleOnChange("age", e)} type="number" max={100} min={5} variant="outlined" />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        Address
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={formValue.address} onChange={e => handleOnChange("address", e)} variant="outlined" multiline
                            rows={4} />
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCloseCustomerCreate} color="primary">
                    CANCEL
                </Button>
                <Button autoFocus onClick={e => createCustomer(formValue)} color="primary">
                    SAVE CHANGES
                </Button>
            </DialogActions>
        </Dialog >
    )
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        createCustomer: (customer) => dispatch(createCustomer(customer))
    }
}

export default connect(() => { }, mapDispatchToProps)(CustomerCreate)