import React, { } from 'react'
import {
    Icon,
    IconButton,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import { connect } from 'react-redux';
import { getCustomers } from 'app/redux/page/customer/CustomerActions';
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& small': {
            height: 15,
            width: 50,
            borderRadius: 500,
            boxShadow:
                '0 0 2px 0 rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.24)',
        },
        '& td': {
            borderBottom: 'none',
        }
    },
}))

function CustomerTable(props) {
    const classes = useStyles()
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, props.customers.length - page * rowsPerPage);

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 5));
        setPage(0);
    };

    return (
        <>
            <Table className={clsx('whitespace-pre min-w-400', classes.productTable)}>
                <TableHead>
                    <TableRow>
                        <TableCell className="px-6" colSpan={2}>
                            Name
                        </TableCell>
                        <TableCell className="px-0" colSpan={2}>
                            Mobile
                        </TableCell>
                        <TableCell className="px-0" colSpan={2}>
                            Age
                        </TableCell>
                        <TableCell className="px-0" colSpan={2}>
                            Gender
                        </TableCell>
                        <TableCell className="px-0" colSpan={1}>
                            Action
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.loading &&
                        <TableRow>
                            <TableCell colSpan={9}>
                                <LinearProgress />
                            </TableCell>
                        </TableRow>}

                    {props.customers
                        .map((customer, index) => (
                            <TableRow key={index} hover>
                                <TableCell
                                    className="px-0 capitalize"
                                    colSpan={2}
                                    align="left"
                                >
                                    <div className="flex items-center">
                                        <p className="m-0">
                                            {customer.name}
                                        </p>
                                    </div>
                                </TableCell>
                                <TableCell className="px-0 capitalize" colSpan={2} align="left"  >
                                    <div className="flex items-center">
                                        <p className="m-0 ">
                                            {customer.mobile}
                                        </p>
                                    </div>
                                </TableCell>

                                <TableCell className="px-0 capitalize" colSpan={2} align="left"  >
                                    <div className="flex items-center">
                                        <p className="m-0">
                                            {customer.age}
                                        </p>
                                    </div>
                                </TableCell>

                                <TableCell className="px-0" align="left" colSpan={2}    >
                                    {customer.gender === "male" ? (
                                        <small className="border-radius-4 bg-secondary text-white px-2 py-2px">
                                            {customer.gender.toUpperCase()}
                                        </small>
                                    ) : (
                                        <small className="border-radius-4 bg-primary text-white px-2 py-2px">
                                            {customer.gender.toUpperCase()}
                                        </small>
                                    )}
                                </TableCell>
                                <TableCell className="px-0 flex" colSpan={1}>
                                    <IconButton>
                                        <Icon color="primary">edit</Icon>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                </TableBody>

            </Table>
        </>
    )
}

export default connect(
    (state, props) => ({
        customers: state.customer.customers,
        loading: state.customer.loading
    }),
    (dispatch, props) => ({
        loadCustomer: dispatch(getCustomers())
    })
)(CustomerTable)
