
import React from 'react'
import { Button, Card } from '@material-ui/core'
import CustomerTable from './shared/CustomerTable'
import CustomerCreate from './shared/CustomerCreate';



export default function Customer() {
    const [customerCreate, setCustomerCreate] = React.useState(false);
    const handleOpenCustomerCreate = () => setCustomerCreate(true);
    const handleCloseCustomerCreate = () => setCustomerCreate(false);

    return (
        <Card elevation={3} className="pt-5 mb-6" style={{ padding: "10px", margin: "10px" }}>

            <div className="flex justify-between items-center px-6 mb-3">
                <span className="card-title">Customers</span>
                <div className="flex items-center" >

                    <span className="flex items-center" >
                        <Button variant="contained" onClick={handleOpenCustomerCreate} color="primary">ADD NEW CUSTOMER</Button>
                    </span>
                </div>
            </div>

            <CustomerTable />
            <CustomerCreate
                customerCreate={customerCreate}
                handleOpenCustomerCreate={handleOpenCustomerCreate}
                handleCloseCustomerCreate={handleCloseCustomerCreate} ></CustomerCreate>
        </Card>
    )
}
