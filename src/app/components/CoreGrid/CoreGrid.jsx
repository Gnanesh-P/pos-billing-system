import React, { } from 'react'
import {
    Icon,
    IconButton,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Button,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import LinearProgress from '@material-ui/core/LinearProgress';


const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& small': {
            height: 15,
            width: 50,
            borderRadius: 500,
            boxShadow:
                '0 0 2px 0 rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.24)',
        },
        '& td': {
            borderBottom: 'none',
        }
    },
}))


export default function CoreGrid(props) {
    const {
        loading = false,
        columns = [],
        data = [],
        customer = {},
        needActions = false,
        actionColspan = 1,
        actionList = [],
        extendColumn = false
    } = props
    const classes = useStyles()

    const renderCel = (row, col) => {
        const value = (col.value && typeof col.value === 'function') ? col.value(row) : row[col.fieldName]
        if (col.isLink && typeof col.onClickLink === 'function') {
            return <a style={{ color: "blue" }} onClick={() => col.onClickLink(row)} > <b>{value}</b> </a>
        }
        else return value;
    }

    const calculateColspan = (colSpan) => extendColumn ? 1 : colSpan;
    const renderCellStyle = (colSpan) => extendColumn ? ({ width: colSpan * 80 }) : ({})

    return (
        <>
            <div style={{ display: "flex", overflow: "auto" }} >
                <Table className={clsx('whitespace-pre min-w-400', classes.productTable)}>
                    <TableHead>
                        <TableRow>
                            {needActions &&
                                <TableCell className="px-6" style={renderCellStyle(actionColspan)} colSpan={calculateColspan(actionColspan)}>
                                    Actions
                                </TableCell>
                            }

                            {columns.map(column =>
                                <TableCell style={renderCellStyle(column.colspan)} colSpan={calculateColspan(column.colspan)}>
                                    {column.displayName}
                                </TableCell>
                            )}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {loading && <TableRow key={-1}>
                            <TableCell colSpan={(columns.reduce((x, y) => x + y.colspan, actionColspan))}>
                                <LinearProgress />
                            </TableCell>
                        </TableRow>}

                        {!loading && data.length == 0 && <TableRow key={'NoDataFound'}>
                            <TableCell colSpan={calculateColspan(columns.reduce((x, y) => x + y.colspan, actionColspan))}>
                                No Data Found
                            </TableCell>
                        </TableRow>}

                        {data.map((rowData, index) => (
                            <TableRow key={index} hover>
                                <TableCell className="px-0 flex" colSpan={calculateColspan(actionColspan)}>
                                    {actionList.map(action => {
                                        if (action.type === "icon") {
                                            switch (action.iconName) {
                                                case "edit": return <IconButton onClick={ev => action.handler(rowData, ev)} >
                                                    <Icon color="primary">edit</Icon>
                                                </IconButton>
                                                case "delete": return <IconButton onClick={ev => action.handler(rowData, ev)} >
                                                    <Icon color="primary">delete</Icon>
                                                </IconButton>
                                                default: return <span></span>
                                            }
                                        }
                                        else if (action.type === "button") {
                                            return <Button variant="contained" onClick={ev => action.handler(rowData, ev)} color="primary"> {action.buttonName}</Button>
                                        } else {
                                            return <span></span>
                                        }
                                    })}
                                </TableCell>

                                {columns.map(col => {
                                    return <TableCell className="px-0 capitalize" colSpan={calculateColspan(col.colspan)} align="left"  >
                                        <div className="flex items-center">
                                            <p className="m-0">
                                                {renderCel(rowData, col)}
                                            </p>
                                        </div>
                                    </TableCell>
                                })}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>
        </>
    )
}
