import React, { useEffect } from 'react'
import { Button } from '@material-ui/core'
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles'
import {
    Icon,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import LinearProgress from '@material-ui/core/LinearProgress';
import TextField from '@material-ui/core/TextField';

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: { padding: theme.spacing(2) },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: { margin: 0, padding: theme.spacing(1) },
}))(MuiDialogActions);

const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& small': {
            height: 15,
            width: 50,
            borderRadius: 500,
            boxShadow:
                '0 0 2px 0 rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.24)',
        },
        '& td': {
            borderBottom: 'none',
        }
    },
    fullWidth: {
        width: "100%",
        "& div": {
            width: "100%"
        },
        '& div>input': {
            width: "100%"
        }
    }
}))

function CoreZoomSelect(props) {
    const {
        size = "md",
        onSelect = () => { },
        title = "Choose Record",
        columns = [],
        data = [],
        loading = false,
        value,
        valueField,
        display,
    } = props

    const classes = useStyles()
    const [open, setOpen] = React.useState(false);
    const [selectedRow, setSelectedRow] = React.useState({});
    const [formValue, setFormValue] = React.useState({});
    const onSelectRow = (row) => {
        onSelect(row)
        setOpen(false)
    }

    const [gridData, setGridData] = React.useState([])
    useEffect(() => {
        let tempFormValue = {}
        columns.forEach(x => { tempFormValue[x.fieldName] = "" })
        setFormValue({ ...tempFormValue })
        setGridData(data)
    }, [data, columns])

    const onSearch = (fieldName, event) => {
        formValue[fieldName] = event.target.value
        setFormValue({ ...formValue })

        const filterData = data.filter(row => columns.every(col => {
            const searchValue = (formValue[col.fieldName] + '').trim().toLocaleLowerCase();
            const rowValue = (row[col.fieldName] + '').trim().toLocaleLowerCase();
            return rowValue.includes(searchValue)
        }))
        setGridData([...filterData])
        console.log(formValue)
    }

    return (
        <>
            <TextField value={display} onClick={() => setOpen(true)} variant="outlined" />
            <Dialog open={open} maxWidth={size}>
                <DialogTitle onClose={() => setOpen(false)}>
                    {title}
                </DialogTitle>
                <DialogContent>
                    <Table className={clsx('whitespace-pre min-w-400', classes.productTable)}>
                        <TableHead>
                            <TableRow>
                                {columns.map(column =>
                                    <TableCell colSpan={column.colspan}>
                                        {column.displayName}
                                    </TableCell>
                                )}
                            </TableRow>
                            <TableRow>
                                {columns.map(column =>
                                    <TableCell colSpan={column.colspan}>
                                        <TextField
                                            value={formValue[column.fieldName]}
                                            onChange={(e) => onSearch(column.fieldName, e)}
                                            className={classes.fullWidth}
                                            placeholder={column.displayName} variant="outlined" />
                                    </TableCell>
                                )}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {loading && <TableRow key={-1}>
                                <TableCell colSpan={columns.reduce((x, y) => x + y.colspan, 0)}>
                                    <LinearProgress />
                                </TableCell>
                            </TableRow>}

                            {!loading && gridData.length == 0 && <TableRow key={'NoDataFound'}>
                                <TableCell colSpan={columns.reduce((x, y) => x + y.colspan, 0)}>
                                    No Data Found
                                </TableCell>
                            </TableRow>}

                            {gridData.map((rowData, index) => (
                                <TableRow
                                    className={rowData[valueField] == selectedRow[valueField] ? "selected" : ""}
                                    key={index} hover
                                    onDoubleClick={() => onSelectRow(rowData)} onClick={() => setSelectedRow(rowData)}  >
                                    {columns.map(col => {
                                        return <TableCell className="px-0 capitalize" colSpan={col.colspan} align="left" >
                                            <div className="flex items-center">
                                                <p className="m-0">
                                                    {(col.value && typeof col.value === 'function') ? col.value(rowData) : rowData[col.fieldName]}
                                                </p>
                                            </div>
                                        </TableCell>
                                    })}
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={() => onSelectRow(selectedRow)} color="primary">
                        OK
                    </Button>
                </DialogActions>
            </Dialog >
        </>


    )
}

export default CoreZoomSelect