import { combineReducers } from 'redux'
import ScrumBoardReducer from './reducers/ScrumBoardReducer'
import NotificationReducer from './reducers/NotificationReducer'
import EcommerceReducer from './reducers/EcommerceReducer'
import NavigationReducer from './reducers/NavigationReducer'
import InventoryReducer from './page/Inventory/InventoryReducer'
import CustomerReducer from './page/customer/CustomerReducer'
import CategoryReducer from 'app/views/category/store/CategoryReducer'
import ProductReducer from 'app/views/product/redux/ProductReducer'
import BillingReducer from './page/billing/BillingReducer'
import UnitReducer from 'app/views/units/store/UnitReducer'
import WarehouseReducer from 'app/views/warehouse/store/WarehouseReducer'
import SupplierReducer from 'app/views/supplier/store/SupplierReducer'
import LocationReducer from 'app/views/location/store/LocationReducer'
import TransactionReducer from 'app/views/transaction/store/TransactionReducer'
import TaxReducer from 'app/views/tax/store/TaxReducer'
import EmployeeReducer from 'app/views/employee/store/EmployeeReducer'

const RootReducer = combineReducers({
    notifications: NotificationReducer,
    navigations: NavigationReducer,
    scrumboard: ScrumBoardReducer,
    ecommerce: EcommerceReducer,
    inventory: InventoryReducer,
    customer: CustomerReducer,
    category: CategoryReducer,
    product: ProductReducer,
    billing: BillingReducer,
    unit: UnitReducer,
    warehouse: WarehouseReducer,
    supplier: SupplierReducer,
    location: LocationReducer,
    transaction: TransactionReducer,
    tax: TaxReducer,
    employee: EmployeeReducer
})

export default RootReducer