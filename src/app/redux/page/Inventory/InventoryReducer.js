import { inventoryActions } from "./InventoryActions"

const initialState = {
    selectedType: 1, // 1=Store,2=Warehouse
    items: [],
    branches: [],
    isInventoryListLoading: false
}

const InventoryReducer = function(state = initialState, action) {
    switch (action.type) {
        case inventoryActions.getAllInventories:
            return {
                ...state,
                items: [...action.payload.items],
                isInventoryListLoading: action.payload.isLoading
            }
        case inventoryActions.collapseWarehouseChildItems:
            return {
                ...state,
                items: [...action.payload],
            }
        case inventoryActions.changeInventoryType:
            return {...state, selectedType: action.payload }
        case inventoryActions.getAllBranch:
            return {...state, branches: [...action.payload] }
        default:
            return {...state }
    }
}

export default InventoryReducer