import axios from 'axios'
import { httpClientGet } from 'axios'
import { utilCopy } from 'utils'

export const inventoryActions = {
    getAllInventories: "inventory/get-all-items",
    changeInventoryType: "inventory/change-type",
    getAllBranch: "inventory/change-branch",
    collapseWarehouseChildItems: "inventory/collapse-warehouse-child-items"
}

export const getInventoryItems = (inventoryType) => (dispatch) => {
    dispatch({
        type: inventoryActions.getAllInventories,
        payload: {
            items: [],
            isLoading: true
        },
    })

    setTimeout(() => {
        const list = [...productList.map(x => {
            if (Array.isArray(x.items) && x.items.length > 0) {
                x.itemCollapse = true
            }
            return x
        })]

        const updateItem = (item, ar) => {
            const parent = ar.find(x => x.product.productId === item.product.productId)
            if (parent) {
                parent.quantity += item.quantity;
                parent.products.push(utilCopy(item))
            } else {
                item.products = [utilCopy(item)]
                ar.push(utilCopy(item))
            }
        }
        const storeItems = []
        const warehouseItems = []
        const items = []
        for (const item of InventoryItems) {
            if (inventoryType === 1 && item.inventoryType === "STORE")
                updateItem(item, items)

            if (inventoryType === 2 && item.inventoryType === "WAREHOUSE")
                updateItem(item, items)
        }

        dispatch({
            type: inventoryActions.getAllInventories,
            payload: {
                items: [...items],
                isLoading: false
            },
        })
    }, 2000)
}

export const getInventoryBranches = type => dispatch => {
    dispatch({
        type: inventoryActions.getAllBranch,
        payload: [
            { name: "chennai-chepac", },
            { name: "chennai-siruseri" },
            { name: "bangalooru" },
            { name: "mumbai" },
        ]
    })
}


export const collapseWarehouseChildItem = (index, items) => dispatch => {
    items[index].itemCollapse = !!!items[index].itemCollapse;
    dispatch({
        type: inventoryActions.collapseWarehouseChildItems,
        payload: items
    })
}

export const changeInventoryType = (type) => (dispatch) => {
    dispatch({
        type: inventoryActions.changeInventoryType,
        payload: type,
    })
}


export const InventoryItems = [{
        "stockId": 1,
        "branchId": "chennai",
        "inventoryType": "STORE",
        "createdDateTime": null,
        "createdUser": "Gnanesh",
        "expiryDate": null,
        "quantity": 35,
        "product": {
            "productId": 1,
            "productBuyingPrice": 10.12,
            "productName": "Soap",
            "productSellingPrice": 20,
            "category": {
                "categoryId": 1,
                "categoryName": "Cosmetics",
                "stocks": []
            },
            "productInvoices": [],
            "productPricings": []
        },
        "supplier": null,
        "category": null
    },
    {
        "stockId": 2,
        "branchId": "chennai",
        "inventoryType": "STORE",
        "createdDateTime": null,
        "createdUser": "Gnanesh",
        "expiryDate": null,
        "quantity": 47,
        "product": {
            "productId": 2,
            "productBuyingPrice": 10.12,
            "productName": "Biscuts",
            "productSellingPrice": 20,
            "category": {
                "categoryId": 1,
                "categoryName": "Cosmetics",
                "stocks": []
            },
            "productInvoices": [],
            "productPricings": []
        },
        "supplier": null,
        "category": null
    },
    {
        "stockId": 3,
        "branchId": "chennai",
        "inventoryType": "STORE",
        "createdDateTime": null,
        "createdUser": "Gnanesh",
        "expiryDate": null,
        "quantity": 23,
        "product": {
            "productId": 2,
            "productBuyingPrice": 10.12,
            "productName": "Biscuts",
            "productSellingPrice": 20,
            "category": {
                "categoryId": 1,
                "categoryName": "Cosmetics",
                "stocks": []
            },
            "productInvoices": [],
            "productPricings": []
        },
        "supplier": null,
        "category": null
    },
    {
        "stockId": 4,
        "branchId": "chennai",
        "inventoryType": "STORE",
        "createdDateTime": null,
        "createdUser": "Gnanesh",
        "expiryDate": null,
        "quantity": 340,
        "product": {
            "productId": 3,
            "productBuyingPrice": 10.12,
            "productName": "Chocklets",
            "productSellingPrice": 20,
            "category": {
                "categoryId": 1,
                "categoryName": "Cosmetics",
                "stocks": []
            },
            "productInvoices": [],
            "productPricings": []
        },
        "supplier": null,
        "category": null
    }, {
        "stockId": 7,
        "branchId": "chennai",
        "inventoryType": "WAREHOUSE",
        "createdDateTime": null,
        "createdUser": "Gnanesh",
        "expiryDate": null,
        "quantity": 35,
        "product": {
            "productId": 1,
            "productBuyingPrice": 10.12,
            "productName": "Soap",
            "productSellingPrice": 20,
            "category": {
                "categoryId": 1,
                "categoryName": "Cosmetics",
                "stocks": []
            },
            "productInvoices": [],
            "productPricings": []
        },
        "supplier": null,
        "category": null
    },
    {
        "stockId": 8,
        "branchId": "chennai",
        "inventoryType": "WAREHOUSE",
        "createdDateTime": null,
        "createdUser": "Gnanesh",
        "expiryDate": null,
        "quantity": 111,
        "product": {
            "productId": 2,
            "productBuyingPrice": 10.12,
            "productName": "Biscuts",
            "productSellingPrice": 20,
            "category": {
                "categoryId": 1,
                "categoryName": "Cosmetics",
                "stocks": []
            },
            "productInvoices": [],
            "productPricings": []
        },
        "supplier": null,
        "category": null
    },
    {
        "stockId": 9,
        "branchId": "chennai",
        "inventoryType": "WAREHOUSE",
        "createdDateTime": null,
        "createdUser": "Gnanesh",
        "expiryDate": null,
        "quantity": 45,
        "product": {
            "productId": 2,
            "productBuyingPrice": 10.12,
            "productName": "Biscuts",
            "productSellingPrice": 20,
            "category": {
                "categoryId": 1,
                "categoryName": "Cosmetics",
                "stocks": []
            },
            "productInvoices": [],
            "productPricings": []
        },
        "supplier": null,
        "category": null
    },
    {
        "stockId": 10,
        "branchId": "chennai",
        "inventoryType": "WAREHOUSE",
        "createdDateTime": null,
        "createdUser": "Gnanesh",
        "expiryDate": null,
        "quantity": 24,
        "product": {
            "productId": 3,
            "productBuyingPrice": 10.12,
            "productName": "Chocklets",
            "productSellingPrice": 20,
            "category": {
                "categoryId": 1,
                "categoryName": "Cosmetics",
                "stocks": []
            },
            "productInvoices": [],
            "productPricings": []
        },
        "supplier": null,
        "category": null
    }
]

const productList = [{
        imgUrl: '/assets/images/products/headphone-2.jpg',
        name: 'earphone',
        price: 100,
        available: 15,
    },
    {
        imgUrl: '/assets/images/products/headphone-3.jpg',
        name: 'earphone',
        price: 1500,
        available: 30,
        items: [{
                name: 'earphone',
                price: 100,
                available: 15,
            }, {
                name: 'earphone',
                price: 100,
                available: 15,
            },
            {
                name: 'earphone',
                price: 100,
                available: 15,
            }
        ]
    },
    {
        imgUrl: '/assets/images/products/iphone-2.jpg',
        name: 'iPhone x',
        price: 1900,
        available: 35,
    },
    {
        imgUrl: '/assets/images/products/headphone-2.jpg',
        name: 'earphone',
        price: 100,
        available: 15,
        items: [{
                name: 'earphone',
                price: 100,
                available: 15,
            }, {
                name: 'earphone',
                price: 100,
                available: 15,
            },
            {
                name: 'earphone',
                price: 100,
                available: 15,
            }
        ]
    },
    {
        imgUrl: '/assets/images/products/headphone-2.jpg',
        name: 'earphone',
        price: 100,
        available: 15,
    },
    {
        imgUrl: '/assets/images/products/headphone-3.jpg',
        name: 'earphone',
        price: 1500,
        available: 30,
    },
    {
        imgUrl: '/assets/images/products/iphone-2.jpg',
        name: 'iPhone x',
        price: 1900,
        available: 35,
    },
    {
        imgUrl: '/assets/images/products/headphone-2.jpg',
        name: 'earphone',
        price: 100,
        available: 15,
    },
    {
        imgUrl: '/assets/images/products/headphone-3.jpg',
        name: 'earphone',
        price: 1500,
        available: 30,
    },
    {
        imgUrl: '/assets/images/products/iphone-2.jpg',
        name: 'iPhone x',
        price: 1900,
        available: 35,
    },
    {
        imgUrl: '/assets/images/products/iphone-1.jpg',
        name: 'iPhone x',
        price: 100,
        available: 0,
    },
    {
        imgUrl: '/assets/images/products/headphone-3.jpg',
        name: 'Head phone',
        price: 1190,
        available: 5,
    },
]