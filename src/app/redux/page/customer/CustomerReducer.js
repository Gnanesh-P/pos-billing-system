import { customerAction } from "./CustomerActions"

const initialState = {
    customers: [],
    loading: false
}

const CustomerReducer = function(state = initialState, action) {
    switch (action.type) {
        case customerAction.getAllCustomer:
            return {
                ...state,
                customers: [...action.payload.customers],
                loading: action.payload.loading
            }
        case customerAction.createCustomer:
            return {
                ...state,
                customers: [...state.customers, {...action.payload }]
            }
        default:
            return {...state }
    }
}

export default CustomerReducer