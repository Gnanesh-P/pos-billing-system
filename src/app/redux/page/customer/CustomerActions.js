import axios from 'axios'
export const customerAction = {
    getAllCustomer: "customer/get-all-customer",
    createCustomer: "customer/create-customer"
}
export const getCustomers = () => (dispatch) => {
    dispatch({
        type: customerAction.getAllCustomer,
        payload: {
            loading: true,
            customers: []
        },
    })
    setTimeout(() => {
        dispatch({
            type: customerAction.getAllCustomer,
            payload: {
                loading: false,
                customers: [...customers]
            },
        })
    }, 2000)
}


export const createCustomer = (customer) => dispatch => {
    setTimeout(() => {
        dispatch({
            type: customerAction.createCustomer,
            payload: customer,
        })
    }, 2000)
}


const customers = [{
        name: "sujith",
        mobile: "98348923",
        gender: "male",
        age: 25
    },
    {
        name: "gnan",
        mobile: "112322",
        gender: "male",
        age: 24
    },
    {
        name: "manjo",
        mobile: "98348923",
        gender: "fe-male",
        age: 26
    }
]