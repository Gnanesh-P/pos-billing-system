import axios from 'axios'
import { utilCopy } from 'utils'

export const billingActions = {
    getAllBillings: "Billing/get-all-billings",
}

export const getAllBillings = () => dispatch => {
    dispatch({
        type: billingActions.getAllBillings,
        payload: {
            items: [],
            loading: true
        }
    })

}