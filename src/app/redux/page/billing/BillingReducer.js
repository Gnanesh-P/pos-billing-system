import { billingActions } from './BillingActions'
const initialState = {
    billings: [],
    loading: false
}

const BillingReducer = function(state = initialState, action) {
    switch (action.type) {
        case billingActions.getAllBillings:
            return {
                ...state,
                billings: [...action.payload.items],
                loading: action.payload.loading
            }
        default:
            return {...state }
    }
}

export default BillingReducer