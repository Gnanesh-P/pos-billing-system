import axios from 'axios'

import * as Axios from './axios-utils';


const axiosInstance = axios.create()

axiosInstance.interceptors.response.use(
    (response) => response,
    (error) =>
    Promise.reject(
        (error.response && error.response.data) || 'Something went wrong!'
    )
)

export default axiosInstance